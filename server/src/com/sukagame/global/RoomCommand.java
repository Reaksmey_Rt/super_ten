package com.sukagame.global;

public class RoomCommand
{
	public static final String CMD_WAITING 							= "CMD_WAITING";
	public static final String CMD_NEW_GAME 						= "CMD_NEW_GAME";
	public static final String CMD_COLLECT_CHIP 					= "CMD_COLLECT_CHIP";
	public static final String CMD_BUY_IN_PANEL_CHANGED 			= "CMD_BUY_IN_PANEL_CHANGED";
	public static final String CMD_BUY_IN 							= "CMD_BUY_IN";
	public static final String CMD_OPEN_CARD 						= "CMD_OPEN_CARD";
	
	public static final String CMD_BUY_JACKPOT 						= "CMD_BUY_JACKPOT";
	public static final String CMD_SHUFFLE_DECK 					= "CMD_SHUFFLE";
	public static final String CMD_DEAL_CARD 						= "CMD_DEAL_CARD";
	public static final String CMD_REVEAL_CARD 						= "CMD_REVEAL_CARD";
	public static final String CMD_BETTING							= "CMD_BETTING";
	public static final String CMD_TURN								= "CMD_TURN";
	public static final String CMD_SORT_CARD 						= "CMD_SORT_CARD";
	public static final String CMD_NEW_ROUND 						= "CMD_NEW_ROUND";
	public static final String CMD_RESULT 							= "CMD_RESULT";
	public static final String CMD_RESULT_TOTAL 					= "CMD_RESULT_TOTAL";
	public static final String CMD_CLEAR 							= "CMD_CLEAR";

	public static final String CMD_RESULT_SHOOT_ANIMATION 			= "CMD_RESULT_SHOOT_ANIMATION";

	public static final String CMD_REQUEST_VALIDATE_HAND 			= "CMD_REQUEST_VALIDATE_HAND";
	public static final String  CMD_REQUEST_CONFIRM_CARDS_RANKED	= "CMD_REQUEST_CONFIRM_CARDS_RANKED"; 
	public static final String  CMD_RESPOND_CONFIRM_CARDS_RANKED	= "CMD_RESPOND_CONFIRM_CARDS_RANKED"; 
	
	public static final String CMD_RESPOND_LEAVE_PENDING 			= "CMD_RESPOND_LEAVE_PENDING";
	public static final String CMD_REQUEST_LEAVE_ROOM 				= "CMD_REQUEST_LEAVE_ROOM";
	
	public static final String CMD_REQUEST_EMOJI 					= "CMD_REQUEST_EMOJI";
	public static final String CMD_RESPOND_EMOJI 					= "CMD_RESPOND_EMOJI";
	
	public static final String CMD_BUY_GAME_JACKPOT 				= "CMD_BUY_GAME_JACKPOT";
	public static final String CMD_WIN_GAME_JACKPOT 				= "CMD_WIN_GAME_JACKPOT";
	
	public static final String CMD_UPDATE_BALANCE 					= "CMD_UPDATE_BALANCE";
	public static final String CMD_WIN_LOST_SPECIAL_AMOUNT 			= "CMD_WIN_LOST_SPECIAL_AMOUNT";
	
	public static final String CMD_REQUEST_STAND 					= "CMD_REQUEST_STAND";
	public static final String CMD_RESPOND_STAND 					= "CMD_RESPOND_STAND";

	public static final String CMD_REQUEST_SEAT 					= "CMD_REQUEST_SEAT";
	public static final String CMD_RESPOND_SEAT 					= "CMD_RESPOND_SEAT";

	public static final String CMD_ERROR 							= "CMD_ERROR";
	public static final String CMD_BROADCAST_MESSAGE 				= "CMD_BROADCAST_MESSAGE";
}