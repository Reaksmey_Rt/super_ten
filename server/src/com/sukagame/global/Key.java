package com.sukagame.global;

public class Key
{
	public static final String KEY_ROOM_TURN 					= "trun";
	public static final String KEY_ROOM_STATE 					= "state";
	public static final String KEY_ROOM_PLAYER 					= "player";
	public static final String KEY_ROOM_PLAYERS 				= "players";
	public static final String KEY_ROOM_PLAYER_SERVER_SEAT 		= "server_seat";
	public static final String KEY_ROOM_PLAYER_SERVER_SEATS 	= "server_seats";
	public static final String KEY_ROOM_PLAYER_CARDS 			= "cards";
	
	public static final String KEY_ROOM_PLAYER_COUNT 			= "player_count";
	public static final String KEY_ROOM_ROUND_COUNT 			= "round_count";
	public static final String KEY_ROOM_GAME_COUNT 				= "game_count";
	
	public static final String KEY_ROOM_TIME 					= "time";
	public static final String KEY_ROOM_MAX_TIME 				= "max_time";
	
	public static final String KEY_ROOM_WAIT_MILI 				= "KEY_ROOM_WAIT_MILI";
	
	public static final String KEY_ROOM_PLAYER_NAME 			= "name";
	public static final String KEY_ROOM_PLAYER_IS_PLAYING 		= "is_playing";
	public static final String KEY_ROOM_PLAYER_IS_DEALER 		= "is_dealer";
	public static final String KEY_ROOM_PLAYER_BALANCE 			= "balance";
	public static final String KEY_ROOM_PLAYER_BUY_IN_AMT 		= "buy_in_amt";
	
	public final static String ACTIVE = "active";
	public static final String KEY_ROOM_MIN_BUY_IN = "min_buy_in";
	public static final String KEY_ROOM_MAX_BUY_IN = "max_buy_in";
	public static final String KEY_BET			= "bet";
	
	public static final String KEY_ROOM_PLAYER_HAND 			= "hand";
	public static final String KEY_ROOM_PLAYER_HAND_DETAIL 		= "hand_detail";

	public static final String KEY_ROOM_CARD_COUNT 				= "card_amt";
	
	public static final String KEY_ROOM_CARD_VALUE 				= "value";
	public static final String KEY_ROOM_CARD_SUIT 				= "suit";

	public static final String KEY_ROOM_ID 						= "room_id";
	public static final String KEY_ROOM_PASSWORD 				= "room_password";
	
	public static final String KEY_PLAYER_ID 					= "player_id";
	public static final String KEY_PLAYER_CODE 					= "player_code";
	public static final String KEY_COMPANY_ID 					= "company_id";
	public static final String KEY_COMPANY_NAME 				= "company_name";
	public static final String KEY_PROVIDER_NAME 				= "provider_name";
	public static final String KEY_AVATAR_ID 					= "avatar_id";
	public static final String KEY_PLAYER_REFERRAL_ID 			= "player_referral_id";
	public static final String KEY_PLAYER_USERNAME 				= "player_username";
	public static final String KEY_PLAYER_NAME 					= "player_name";
	public static final String KEY_PLAYER_SALT 					= "player_salt";
	public static final String KEY_PLAYER_PIN_CODE 				= "player_pin_code";
	public static final String KEY_SESSION_IS_ROBOT 			= "session_is_robot";
	public static final String KEY_PLAYER_BALANCE 				= "player_balance";

	public static final String KEY_PASSWORD 					= "password";
	public static final String KEY_USERS 						= "users";
	
	public static final String KEY_ROOM_NAME 					= "room_name";
	public static final String KEY_ROOM_DETAIL_ID 				= "room_detail_id";
	
	public static final String KEY_PARAM 						= "param";
	public static final String ROOM_PLAYER_COUNT 				= "player_count";
}
