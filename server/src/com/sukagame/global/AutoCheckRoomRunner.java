package com.sukagame.global;

import com.sukagame.superten.database.GameDatabaseManager;
import com.sukagame.superten.database.model.Room;
import com.sukagame.superten.util.GameRoom;
import com.sukagame.superten.util.Util;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class AutoCheckRoomRunner implements Runnable{

    private AtomicReference<GameManager> gameManagerAtomicReference;
    private String[] roomTypeNames = {"SMALL","MEDIUM","HIGH","VIP"};

    public  AutoCheckRoomRunner(AtomicReference<GameManager> gameManagerAtomicReference)
    {
        this.gameManagerAtomicReference = gameManagerAtomicReference;
    }

    @Override
    public void run()
    {
        //System.out.println("============================================ susun room runner");
        if(gameManagerAtomicReference.get().roomsDatabase != null && gameManagerAtomicReference.get().roomsDatabase.size() > 0)
        {
            for(int i = 0; i < roomTypeNames.length;i++)
            {
                String roomTypeName = roomTypeNames[i];
                if(isRoomTypeFulled(roomTypeName))
                {
                    Room room = getRoom(roomTypeName);
                    if(room != null)
                    {
                        //System.out.println("============================================ room full. create new room");
                        gameManagerAtomicReference.get().roomAutoCount +=1;
                        room.name = "ROOM-SG" + gameManagerAtomicReference.get().roomAutoCount;
                        int noId = new GameDatabaseManager().getLatestId(GameDatabaseManager.STORE_PROCEDURE.GET_ROOM_LATEST_ID);
                        room.id = Util.getFormatNumber("R", noId);
                        room.jackpotLimits = new GameDatabaseManager().getJackpotLimits(room.roomTypeId);
                        GameRoom.create(room, gameManagerAtomicReference.get().api,gameManagerAtomicReference.get().zone, null);
                    }
                }
            }
        }
    }

    private Room getRoom(String roomTypeName)
    {
        for(int i = 0; i < gameManagerAtomicReference.get().roomsDatabase.size();i++)
        {
            Room room = gameManagerAtomicReference.get().roomsDatabase.get(i);
            if(room.roomTypeName.equals(roomTypeName))
            {
                return room;
            }
        }
        return  null;
    }

    private boolean isRoomTypeFulled(String roomTypeName)
    {
        List<com.smartfoxserver.v2.entities.Room> rooms = gameManagerAtomicReference.get().zone.getRoomListFromGroup("superten")
        		.stream().filter(r -> r.getVariable("room_type_name").getStringValue()
        				.equals(roomTypeName) && r.getVariable("state").getStringValue()
        				.equals("WAITING")).collect(Collectors.toList());
        if(rooms != null && rooms.size() > 0)
            return  false;
        return  true;
    }
}
