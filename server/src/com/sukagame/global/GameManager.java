package com.sukagame.global;

import com.smartfoxserver.v2.api.ISFSApi;
import com.smartfoxserver.v2.entities.Zone;
import com.sukagame.superten.database.model.Room;
//import com.sukagame.superten.gamelogic.Player;
import com.sukagame.superten.gamelogic.SeatManager;
import com.sukagame.superten.room.RoomExtension;
//import com.sukagame.superten.send.Seat;

import java.util.List;
//import java.util.concurrent.Executors;
//import java.util.concurrent.ScheduledExecutorService;
//import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

public class GameManager
{
//	public enum BetTypes {
//		NONE,
//		CHECK,
//		ALL_IN,
//		CALL,
//		BET,
//		RAISE,
//		FOLD
//	}
	private static GameManager instance = null;
	public Zone zone;
	public ISFSApi api;
	public RoomExtension roomExtension;
	public List<Room> roomsDatabase;

	public int maxPlayer = 8;
	public  int roomAutoCount = 0;
	public SeatManager seatManager;
	
	private GameManager() {}
	
	public static GameManager getInstance()
	{
		if(instance == null)
		{
			instance = new GameManager();
			instance.init();
		}
		return instance;
	}

	private void init()
	{
		AtomicReference<GameManager> gameManagerAtomicReference = new AtomicReference<>();
		gameManagerAtomicReference.set(this);
	//	ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
	//	executor.scheduleAtFixedRate(new AutoCheckRoomRunner(gameManagerAtomicReference), 0, 1, TimeUnit.MINUTES);
	}
}
