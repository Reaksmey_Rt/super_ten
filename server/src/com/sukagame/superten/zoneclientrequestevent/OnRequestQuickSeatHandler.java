package com.sukagame.superten.zoneclientrequestevent;

import java.util.List;
//import java.util.Random;
//import java.util.stream.Collectors;

import com.sukagame.global.GameManager;
import com.sukagame.superten.database.model.PlayerInfo;
import com.sukagame.superten.error.CustomErrorCode;
import com.sukagame.superten.util.GameRoom;
import com.sukagame.superten.util.GameUtil;
import com.sukagame.superten.util.Util;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;

public class OnRequestQuickSeatHandler extends BaseSukaZoneClientRequestHandler 
{	
	@Override
	public void handleClientRequest(User user, ISFSObject params) {
		super.handleClientRequest(user, params);
		PlayerInfo playerInfo = GameUtil.getPlayerInfo(user);
		//System.out.print(" Hello !!!");
		Room room = getAvailableRoom(playerInfo);
		if(room != null && GameUtil.isGameActive(user))
			GameRoom.join(user,room, "");
		else 
			Util.sendErrorCode(CustomErrorCode.All_ROOM_FULL.getId(), user);
	}

	private Room getAvailableRoom(PlayerInfo playerInfo)
	{
//		int maxCountFilter = playerInfo.isRobot() ? 3 : 4;
		List<Room> rooms = GameManager.getInstance().zone.getRoomListFromGroup("superten");
//				.stream()
//				.filter(r -> r.getVariable("player_count").getIntValue() < maxCountFilter && r.getPlayersList().size() < maxCountFilter
//				&& playerInfo.getBalance() >= r.getVariable("min_balance")
//				.getDoubleValue()).collect(Collectors.toList());
	
		if(rooms.size() > 0)
			return rooms.get(0);
		return null;
	}
}
