package com.sukagame.superten.zoneclientrequestevent;

import com.sukagame.global.GameManager;
import com.sukagame.global.Key;
import com.sukagame.superten.util.GameRoom;
import com.sukagame.superten.util.GameUtil;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;

public class OnRequestJoinRoomHandler extends BaseSukaZoneClientRequestHandler
{
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		super.handleClientRequest(user, params);
		Integer roomId = params.getInt(Key.KEY_ROOM_ID);
		String password = params.getUtfString(Key.KEY_ROOM_PASSWORD);
		password = password.equals("") ? null : password;
		Room room =  GameManager.getInstance().zone.getRoomById(roomId);
		if(GameUtil.isGameActive(user) && room != null)
			GameRoom.join(user, room, password);
	}
}
