package com.sukagame.superten.zoneclientrequestevent;

import com.sukagame.global.GameManager;
import com.sukagame.global.Key;
import com.sukagame.superten.database.GameDatabaseManager;
import com.sukagame.superten.database.SukaDatabaseManager;
import com.sukagame.superten.database.GameDatabaseManager.STORE_PROCEDURE;
import com.sukagame.superten.database.model.PlayerInfo;
import com.sukagame.superten.database.model.Room;
import com.sukagame.superten.database.model.RoomDetailData;
import com.sukagame.superten.error.CustomErrorCode;
import com.sukagame.superten.util.GameRoom;
import com.sukagame.superten.util.GameUtil;
import com.sukagame.superten.util.Util;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;

public class OnRequestCreateRoomHandler extends BaseSukaZoneClientRequestHandler
{
	com.smartfoxserver.v2.entities.Room createdRoom;
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		super.handleClientRequest(user, params);
		PlayerInfo playerInfo = GameUtil.getPlayerInfo(user);
		int noId = new GameDatabaseManager().getLatestId(STORE_PROCEDURE.GET_ROOM_LATEST_ID);
		String roomId = Util.getFormatNumber("R", noId);
		String roomName = params.getUtfString(Key.KEY_ROOM_NAME);
		String password = params.getUtfString(Key.KEY_PASSWORD);
		int roomDetailId = params.getInt(Key.KEY_ROOM_DETAIL_ID);
		RoomDetailData roomDetailData = new SukaDatabaseManager().getRoomDetailData(roomDetailId);
		String ownerId = playerInfo.getId();
		Double playerBalance = getBalance(playerInfo, user);
		
		if(playerBalance >= roomDetailData.minBalance)
		{
			Room room = new Room(roomId, 
									roomDetailData.roomTypeId, 
									roomDetailData.name, 
									roomDetailData.roomLimitTypeId, 
									roomDetailData.limitName, 
									roomName, 
									ownerId, 
									password, 
									1, 
									roomDetailData.stake, 
									false, 
									roomDetailData.minBalance,
									roomDetailData.buyInMinAmount,
									roomDetailData.buyInMaxAmount,
									roomDetailData.betMinAmount,
									true,
									"G000001");
			room.jackpotLimits = new GameDatabaseManager().getJackpotLimits(roomDetailData.roomTypeId);
			user.setProperty("create_room", true);
			createdRoom = GameRoom.create(room,getApi(),GameManager.getInstance().zone, user);
		}
		else 
		{
			Util.sendErrorCode(CustomErrorCode.NOT_ENOUGH_MONEY_TO_CREATE_ROOM.getId(), user,Double.toString(roomDetailData.minBalance));
		}
	}
	
	private double getBalance(PlayerInfo playerInfo,User user)
	{
		return GameUtil.getPlayerInfo(user).getBalance();
	}
}
