package com.sukagame.superten.gamelogic;

import com.sukagame.global.Key;
import com.sukagame.superten.database.*;
import com.sukagame.superten.database.model.*;
import com.sukagame.superten.room.RoomExtension;
import com.sukagame.superten.send.Jackpot;
import com.sukagame.superten.send.UpdateBalance;
import com.sukagame.superten.util.GameUtil;
import com.sukagame.superten.util.Util;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public abstract class Player implements Comparable<Player> {
	private Hand currentHand;

	@Override
	public int compareTo(Player p) {
		return getServerSeat() - p.getServerSeat();
	}

	public int requestSeat = -1;
	public int serverSeat = -1;
	private boolean isPlaying = false;
	private boolean isDealer = false;
	private boolean isRequestStand = false;
	private boolean isRequestLeave = false;
	private boolean isWinSpecialCombination;
	private boolean isWinJackpotCombination;
	private double buyInAmount = 0;

	public JackpotData currentJackpotData = null;
	public JackpotData nextJackpotData = null;

	public RoomExtension roomExtension;
	public PlayerController playerController;
	public Room room;

	private User user;
	private PlayerInfo playerInfo;
	private Table table;
	private SukaDatabaseManager zoneDatabaseManager;
	private GameDatabaseManager databaseManager;
	private JackpotLimit jackpotLimit = null;

	public double rateShareByCompanyAndGame = 0d;
	public Pay pay;
	

	public Player() {
		this.currentJackpotData = new JackpotData();
		this.nextJackpotData = new JackpotData();
		this.currentHand = new Hand(this);
		this.zoneDatabaseManager = new SukaDatabaseManager();
		this.databaseManager = new GameDatabaseManager();
	}

	public Player(PlayerController playerController, Room room) {
		this.playerController = playerController;
		this.roomExtension = playerController.getRoomExtension();
		this.room = room;

		this.currentHand = new Hand(this);
		this.zoneDatabaseManager = new SukaDatabaseManager();
		this.databaseManager = new GameDatabaseManager();
	}

	public abstract void init(int maxTime);

	public abstract void ranking(int currentTime);

	public abstract void confirm(int currentTime);

	public void processPay(double amount) {
		if (getTable().isWalletSeamless)
			deductBalance(amount);
		else
			deductBalance(amount);
	}

	public void processPay() {
		processPay(pay.getFinalAmount());
	}

	public String getId() {
		return playerInfo.getId();
	}

	public String getName() {
		return playerInfo.getName();
	}

	public String getUsername() {
		return playerInfo.getUsername();
	}

	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return this.user;
	}

	public void setTable(Table table) {
		this.table = table;
		this.pay = new Pay(table, this);
	}

	public Table getTable() {
		return this.table;
	}

	public boolean isNotEnoughBalanceToPlayNext() {
		// if(getBalance() < getTable().room.minBalance)
		// return true;
		return false;
	}

	public void setPlayerInfo(PlayerInfo playerInfo) {
		this.playerInfo = playerInfo;
		user.setName(playerInfo.getUsername());
		this.setUser(user);
		this.setBalance(playerInfo.getBalance());

		rateShareByCompanyAndGame = zoneDatabaseManager.getShareByCompanyAndGame(playerInfo.getCompanyId());
	}

	public PlayerInfo getPlayerInfo() {
		return this.playerInfo;
	}

	public boolean isSelf(Player player) {
		return this.playerInfo.getId().equals(player.getId());
	}

	public void setIsWinSpecialCombination(boolean isSpecialCombine) {
		this.isWinSpecialCombination = isSpecialCombine;
	}

	public boolean getIsWinSpecialCombination() {
		return isWinSpecialCombination;
	}

	public void setIsWinJackpotCombination(boolean isWinJackpot) {
		this.isWinJackpotCombination = isWinJackpot;
	}

	public boolean getIsWinJackpotCombination() {
		return isWinJackpotCombination;
	}

	public void setIsPlaying(boolean isPlaying) {
		this.isPlaying = isPlaying;
	}

	public void setIsDealer(boolean isdealer) {
		this.isDealer = isdealer;
	}

	public boolean getIsDealer() {
		return this.isDealer;
	}

	public void setBalance(double balance) {
		getUser().getSession().setProperty(Key.KEY_PLAYER_BALANCE, balance);
		if (table != null && table.roomExtension != null)
			new UpdateBalance(table.roomExtension).send(this);
		GameUtil.setBalance(user);
	}

	public double getBalance() {
		return GameUtil.getPlayerInfo(user).getBalance();
	}

	public void setBetting(double amount) {
		table.bet_amount = amount;
//		getUser().getSession().setProperty(Key.KEY_PLAYER_BALANCE, balance);
//		if(table != null && table.roomExtension != null)
//			new UpdateBalance(table.roomExtension).send(this);
//		GameUtil.setBalance(user);
	}

	public double getBetting() {
		return table.bet_amount;
	}

	public void setBuyInAmount(double buyInAmount) {
		this.buyInAmount = buyInAmount;
	}

	public double getBuyInAmount() {
		return this.buyInAmount;
	}

	public boolean getIsPlaying() {
		if (getServerSeat() == -1)
			return false;
		return this.isPlaying;
	}

	public Hand getHand() {
		return currentHand;
	}

	public void setServerSeat(int num) {
		serverSeat = num;
	}

	public int getServerSeat() {
		return serverSeat;
	}

	public void setRequestSeat(int num) {
		//System.out.println("---------------- Request seat : " + num);
		requestSeat = num;
	}

	public int getRequestSeat() {
		return requestSeat;
	}

	public boolean isRobot() {
		return playerInfo.isRobot();
	}

	public void setIsRequestStand(boolean isRequestStand) {
		this.isRequestStand = isRequestStand;
	}

	public boolean getIsRequestStand() {
		return this.isRequestStand;
	}

	public void setIsRequestLeave(boolean isRequestLeave) {
		this.isRequestLeave = isRequestLeave;
	}

	public boolean getIsRequestLeave() {
		return this.isRequestLeave;
	}

	public void buyJackpotNextRound(int jackpotId) {
		if (jackpotId > 0 && jackpotId < 4) {
			jackpotLimit = table.room.jackpotLimits.get(jackpotId - 1);
			nextJackpotData = new JackpotData(jackpotId, jackpotLimit.amount, true);
		} else {
			nextJackpotData = new JackpotData();
		}
	}

	public void buyJackpot() {
		if (currentJackpotData.jackpotId != -1) {
			jackpotLimit = table.room.jackpotLimits.get(currentJackpotData.jackpotId - 1);
			if (jackpotLimit != null) {
				double remainBalance = getBalance() - jackpotLimit.amount;
				if (remainBalance >= table.room.minBalance) {
					deductBalance(-jackpotLimit.amount);
					AddBuyJackpot(jackpotLimit.amount);

					// new UpdateBalance(roomExtension).send(this);
					new Jackpot(roomExtension).sendBuyJackpot(this, currentJackpotData.jackpotId,
							currentJackpotData.jackpotId, jackpotLimit.amount, "");
					ISFSObject param = new SFSObject();
					param.putDouble("amount", jackpotLimit.amount);
					getRoomExtension().getParentZone().getExtension().handleInternalMessage("jackpot", param);
				} else {
					new Jackpot(roomExtension).sendBuyJackpot(this, -1, -1, 0, "Not enough balance.");
				}
			}
		} else {
			new Jackpot(roomExtension).sendBuyJackpot(this, -1, -1, 0, "");
		}
	}

	public void deductWinJackpotReward(double winJackpotAmount) {
		if (currentJackpotData.isBuyJackpot) {
			zoneDatabaseManager.updateProgressiveJackpot(-winJackpotAmount);
			double balance = getBalance();
			double lastBalance = balance + winJackpotAmount;
			getUser().getSession().setProperty(Key.KEY_PLAYER_BALANCE, lastBalance);
			GameUtil.setBalance(user);
			AddJackpotWinner(winJackpotAmount);
		}
	}

	public void addCard(Card card) {
		getHand().addCardToHand(card);
	}

	public void deductBalance(double amount) {
		double balance = getBalance();
		double lastBalance = balance + amount;
		setBalance(lastBalance);
	}

	public void clear() {
		getHand().clear();
		pay = new Pay(table, this);
		isWinSpecialCombination = false;
		isWinJackpotCombination = false;
		currentJackpotData.jackpotRewardedAmount = 0;
		nextJackpotData.jackpotRewardedAmount = 0;
		// if(!this.getIsPlaying() && this.getServerSeat() != -1)
		// this.setIsPlaying(true);
	}

	public RoomExtension getRoomExtension() {
		return this.roomExtension;
	}

	public PlayerController getPlayerController() {
		return this.playerController;
	}

	public Room getRoom() {
		return this.room;
	}

	public void AddBalanceHistory(String ref) {
		BalanceHistory balanceHistory = new BalanceHistory();
		balanceHistory.id = ref;
		balanceHistory.companyId = playerInfo.getCompanyId();
		balanceHistory.playerId = getId();
		balanceHistory.amount = GameUtil.round(pay.getFinalAmount(), 4);
		balanceHistory.type = balanceHistory.amount > 0 ? "DEPOSIT" : "WITHDRAWAL";
		balanceHistory.lastBalance = GameUtil.round(getBalance(), 4);
		balanceHistory.ip = user.getIpAddress();
		databaseManager.addBalanceHistory(balanceHistory);
	}

	public void AddBalanceTransaction(String ref) {
		BalanceTransaction transaction = new BalanceTransaction();
		transaction.id = ref;
		transaction.companyId = playerInfo.getCompanyId();
		transaction.playerId = getId();
		transaction.roomId = table.roomId;
		transaction.gamePlayId = table.gamePlayId;
		transaction.amount = GameUtil.round(pay.getFinalAmount(), 4);
		transaction.lastBalance = GameUtil.round(getBalance(), 4);
		transaction.ip = user.getIpAddress();
		databaseManager.addBalanceTransaction(transaction);
	}

	public void AddGamePlayResult() {
		java.util.List<String> providers = table.getPlayerController().getActiveProvider();

		PlayBetResult playBetResult = new PlayBetResult();
		playBetResult.gamePlayId = table.gamePlayId;
		playBetResult.companyId = playerInfo.getCompanyId();
		playBetResult.companyName = playerInfo.getCompanyName();
		playBetResult.playerId = getId();
		playBetResult.username = getUsername();
		playBetResult.isRobot = isRobot();
		playBetResult.roomId = table.roomId;
		playBetResult.roomName = table.room.name;
		playBetResult.round = table.gamePlayCode;
		playBetResult.isWinSpecial = isWinSpecialCombination ? 1 : 0;
		playBetResult.stake = room.stake;
		playBetResult.totalBet = Math.abs(GameUtil.round(pay.getRemainAmount(), 4));
		playBetResult.turnover = Math.abs(GameUtil.round(pay.getRemainAmount(), 4));
		playBetResult.prize = GameUtil.round(pay.getRemainAmount(), 4);
		playBetResult.fee = GameUtil.round(pay.getFee(), 4);
		playBetResult.shareCapital = GameUtil.round(pay.getShareCapital(), 4);
		playBetResult.commission = GameUtil.round(pay.getCommission(), 4);
		playBetResult.amount = GameUtil.round(pay.getFinalAmount(), 4);
		playBetResult.status = playBetResult.prize > 0 ? "WIN" : playBetResult.prize == 0 ? "DRAW" : "LOST";
		playBetResult.jackpotBuyAmount = currentJackpotData.jackpotAmount;
		playBetResult.jackpotWinAmount = currentJackpotData.jackpotRewardedAmount;
		playBetResult.lastBalance = GameUtil.round(getBalance(), 4);
		playBetResult.providerName = playerInfo.getProviderName();

		databaseManager.AddPlayerBetResult(playBetResult);
		new ProviderDatabaseManager().AddBetReport(playBetResult, providers);

		String ref = Util.getBalanceHistoryId(getId(), table.gameId);

		AddBalanceHistory(ref);
		AddBalanceTransaction(ref);
	}

	public void AddBuyJackpot(double amount) {
		BuyJackpot buyJackpot = new BuyJackpot();
		buyJackpot.gamePlayId = table.gamePlayId;
		buyJackpot.roomId = table.roomId;
		buyJackpot.companyId = playerInfo.getCompanyId();
		buyJackpot.playerId = getId();
		buyJackpot.roundId = table.gamePlayCode;
		buyJackpot.buyAmount = GameUtil.round(amount, 4);
		buyJackpot.lastBalance = GameUtil.round(getBalance(), 4);
		databaseManager.addBuyJackpot(buyJackpot);
	}

	public void AddJackpotWinner(double rewardedAmount) {

	}

//	public void Betting(double amount)
//	{
//		double balance = getBalance();
//		double lastBalance = balance - amount;
//		setBalance(lastBalance);
//	}

	public ISFSObject toSFSObjectHideCard() {
		ISFSObject param = new SFSObject();
		param.putInt(Key.KEY_ROOM_PLAYER_SERVER_SEAT, serverSeat);
		param.putInt(Key.KEY_AVATAR_ID, playerInfo.getAvatarId());
		param.putUtfString(Key.KEY_ROOM_PLAYER_NAME, getUsername());
		param.putBool(Key.KEY_ROOM_PLAYER_IS_PLAYING, isPlaying);
		param.putDouble(Key.KEY_ROOM_PLAYER_BALANCE, getBalance());
		param.putBool("is_win_jackpot", getIsWinJackpotCombination());
		param.putBool("is_win_special", getIsWinSpecialCombination());
		param.putSFSObject(Key.KEY_ROOM_PLAYER_HAND, currentHand.toSFSObjectHideCard());
		return param;
	}

	public ISFSObject toSFSObjectDealCard(Integer seat) {
		ISFSObject param = new SFSObject();
		param.putInt(Key.KEY_ROOM_PLAYER_SERVER_SEAT, getServerSeat());
		param.putInt(Key.KEY_AVATAR_ID, playerInfo.getAvatarId());
		param.putUtfString(Key.KEY_ROOM_PLAYER_NAME, getUsername());
		param.putBool(Key.KEY_ROOM_PLAYER_IS_PLAYING, isPlaying);
		param.putBool(Key.KEY_ROOM_PLAYER_IS_DEALER, getIsDealer());
		param.putDouble(Key.KEY_ROOM_PLAYER_BALANCE, getBalance());
		param.putSFSObject(Key.KEY_ROOM_PLAYER_HAND,
				(seat == getServerSeat()) ? currentHand.toSFSObjectDealCard() : currentHand.toSFSObjectHideCard());
		return param;
	}

	public ISFSObject toSFSObjectSelf() {
		ISFSObject param = new SFSObject();
		param.putInt(Key.KEY_ROOM_PLAYER_SERVER_SEAT, serverSeat);
		param.putInt(Key.KEY_AVATAR_ID, playerInfo.getAvatarId());
		param.putUtfString(Key.KEY_ROOM_PLAYER_NAME, getUsername());
		param.putBool(Key.KEY_ROOM_PLAYER_IS_PLAYING, isPlaying);
		param.putDouble(Key.KEY_ROOM_PLAYER_BALANCE, getBalance());
		param.putDouble(Key.KEY_ROOM_PLAYER_BUY_IN_AMT, getBuyInAmount());
		param.putBool("is_win_jackpot", getIsWinJackpotCombination());
		param.putBool("is_win_special", getIsWinSpecialCombination());
		param.putSFSObject(Key.KEY_ROOM_PLAYER_HAND, currentHand.sentSFSObjectCards());
		return param;
	}

	public ISFSObject toSFSObjectAllPlayer() {
		ISFSObject param = new SFSObject();
		param.putInt(Key.KEY_ROOM_PLAYER_SERVER_SEAT, serverSeat);
		param.putInt(Key.KEY_AVATAR_ID, playerInfo.getAvatarId());
		param.putUtfString(Key.KEY_ROOM_PLAYER_NAME, getUsername());
		param.putBool(Key.KEY_ROOM_PLAYER_IS_PLAYING, isPlaying);
		param.putBool(Key.KEY_ROOM_PLAYER_IS_DEALER, getIsDealer());
		param.putDouble(Key.KEY_ROOM_PLAYER_BALANCE, getBalance());
		param.putDouble(Key.KEY_ROOM_PLAYER_BUY_IN_AMT, getBuyInAmount());
		param.putDouble(Key.KEY_BET, getBetting());
		return param;
	}

}
