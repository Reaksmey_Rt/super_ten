package com.sukagame.superten.gamelogic;

import com.sukagame.superten.gamelogic.GameTemplate.STATE;
import com.sukagame.superten.room.RoomExtension;
import com.sukagame.superten.util.GameUtil;

public class Game {
	private Table table;
	private GameTemplate gameTemplate;
	private RoomExtension roomExtension;
	public boolean isGameInactive = false;

	public Game(GameTemplate gameTemplate) {
		table = new Table(gameTemplate);
		this.gameTemplate = gameTemplate;
		this.roomExtension = gameTemplate.getRoomExtension();
	}

	public void Waiting() {
	
		table.waiting();
		GameUtil.wait(5, roomExtension);
	}

	public void newGame() {
		System.out.println("_______ New Game ");
	
		table.newGame();

		GameUtil.wait(1, roomExtension);
		gameTemplate.setState(STATE.COLLECT_CHIP);
	}

	public void collectChip() {
		System.out.println("________ Collect Chip ");
		table.collectChip();

		GameUtil.wait(2, roomExtension);
		gameTemplate.setState(STATE.SHUFFLE_DECK);
		// gameTemplate.setState(STATE.JACKPOT);
	}

	public void Jackpot() {
		//System.out.println("Jackpot ");
		table.jackpot();
		GameUtil.wait(3, roomExtension);
		gameTemplate.setState(STATE.SHUFFLE_DECK);
	}

	public void shuffleDeck() {
		System.out.println("________ ShuffleDeck");
		table.shuffleDeck();
		GameUtil.wait(1, roomExtension);
		gameTemplate.setState(STATE.DEAL_2_CARD);
	}

	public void dealCard(int cardAmt) {
		System.out.println("_____ DealCard " + cardAmt);

		table.deck.deal_card_amt = cardAmt;
		table.dealCard(cardAmt);
		GameUtil.wait(3, roomExtension);
		gameTemplate.setState(STATE.REVEAL_CARD);

	}

	public void revealCard() {

		int cardAmt = table.deck.deal_card_amt;
		// TODO Auto-generated method stub
		System.out.println("_____ Reveal Card " + cardAmt);
		table.revealCard(cardAmt);

		// if(cardAmt == 2) {
		GameUtil.wait(3, roomExtension);
		gameTemplate.setState(STATE.OPEN_CARD);

	}

	public void openCard() {
		
		System.out.println("_____ Open Card " );
		//int cardAmt = table.deck.deal_card_amt;
		table.openCard(table.deck.cards_amt);

		GameUtil.wait(2, roomExtension);
		gameTemplate.setState(STATE.BETTING);

	}

	public void bettingTurn() {
		System.out.println("_____ Betting ");
		table.RunTurn();

		if (table.deck.deal_card_amt == 2) {
			GameUtil.wait(1, roomExtension);
			gameTemplate.setState(STATE.DEAL_1_CARD);
		} else {
			GameUtil.wait(1, roomExtension);
			gameTemplate.setState(STATE.RESULT);
		}		

	}



	public void Result() {
		System.out.println("_____ Result ");
		table.gameResult();

//		if(isGameInactive)
//		{
//			List<User> users = roomExtension.getGameRoom().getUserList();
//			for (User user : users)
//			{
//				if(!GameUtil.isGameActive(user))
//				{
//					getPlayerController().Remove(getPlayerController().getPlayerById(GameUtil.getPlayerInfo(user).getId()));
//					roomExtension.getApi().leaveRoom(user, roomExtension.getGameRoom(), true, true);
//				}
//			}
//			isGameInactive = false;
//		}
		GameUtil.wait(5, roomExtension);
		gameTemplate.setState(STATE.CLEAR);
	}

	public void Stop() {
		table.stop();
		GameUtil.wait(1, roomExtension);
		gameTemplate.setState(STATE.NEW_GAME);
	}

	public PlayerController getPlayerController() {
		return roomExtension.getPlayerController();
	}

	public Table getTable() {
		return this.table;
	}

}
