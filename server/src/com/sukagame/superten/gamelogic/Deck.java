package com.sukagame.superten.gamelogic;

import java.util.List;

import com.sukagame.superten.database.model.Room;

public class Deck
{
	public int cards_amt = 52;
	public int deal_card_amt = 2;
	List<Card> deck;

	private int cardsUsed;
	private Room room;

	public Deck(Room room) {
		this.room = room;
		deck = Card.createOrderedDeck(room);
		cardsUsed = 0;
	}

	public void shuffle() {
		deck = Card.createShuffledDeck(room);
		cardsUsed = 0;
		
	}

	public int cardsLeft() {
		return deck.size() - cardsUsed;
	}

	public Card dealCard() {
		cardsUsed++;
		return deck.get(cardsUsed-1);
	}
}
