package com.sukagame.superten.gamelogic;

import com.sukagame.superten.database.model.Room;

public class ComputerPlayer extends Player
{
	public ComputerPlayer(PlayerController playerController,Room room)
	{
		this.playerController = playerController;
		this.roomExtension = playerController.getRoomExtension();
		this.room = room;
	}
	
	@Override
	public void init(int maxTime)
	{

	}

	@Override
	public void ranking(int currentTime)
	{

	}

	@Override
	public void confirm(int currentTime)
	{

	}
	
}
