package com.sukagame.superten.gamelogic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
//import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicReference;
//import java.util.stream.Collectors;
import java.util.stream.Collectors;

import com.sukagame.global.Key;
import com.sukagame.superten.database.model.JackpotData;
import com.sukagame.superten.database.model.PlayerInfo;
import com.sukagame.superten.database.model.Room;
import com.sukagame.superten.gamelogic.GameTemplate.STATE;
import com.sukagame.superten.room.RoomExtension;
import com.sukagame.superten.send.*;
import com.sukagame.superten.util.GameUtil;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.variables.RoomVariable;
import com.smartfoxserver.v2.entities.variables.SFSRoomVariable;

public class PlayerController {
	private Room room;
	private List<Player> players;
	private List<User> waitReconnectUsers = new ArrayList<User>();
	private AtomicReference<RoomExtension> roomExtensionAtomic;

	// public int minPlayer = 2;
	// public int maxPlayer = 8;

	public PlayerController(AtomicReference<RoomExtension> roomExtensionAtomic) {
		this.roomExtensionAtomic = roomExtensionAtomic;
		this.players = new ArrayList<Player>();
		room = GameUtil.getRoom(getRoomExtension().getGameRoom());
	}

	public void join(User user) {
		try {
			PlayerInfo playerInfo = GameUtil.getPlayerInfo(user);
			Player played = getPlayerById(playerInfo.getId());

			// System.out.println("-------------------------------------------------------------");
			// System.out.println("playerInfo _: "+ playerInfo.getUsername());
			// System.out.println("played _: "+ played.getName());

			if (played != null) {
				played.setUser(user);
				// played.setIsPlaying(true);

				new PlayerJoin(getRoomExtension()).sendSelf(played);
				new PlayerJoin(getRoomExtension()).sendAll();

				/////////////////////////////////////////////////////////////
				if (played.getIsRequestLeave()) {
					new General(getRoomExtension()).SendRespondLeavePending(played);
				}

				if (played.getIsRequestStand()) {
					new Stand(getRoomExtension()).sendMessage(played, false);
				}

			} else {
				Player player = GameUtil.createPlayer(playerInfo, user, this, room);

				addPlayer(player); // add player first joint
				if (player.isRobot()) {
					int seat = isRoomCreator(user) ? 0 : getTable().seatManager.getSeat();
					int activePlayerCount = getActivePlayers().size();
					if (seat == -1 || activePlayerCount >= room.maxPlayer || isPlayerExist(player.getId())) {
						GameUtil.leaveRoom(user, getRoomExtension());
						return;
					}

					if (player.getUser().isSpectator())
						getRoomExtension().getApi().spectatorToPlayer(player.getUser(),
								getRoomExtension().getGameRoom(), false, true);
					if (!players.contains(player)) {
						players.add(player);
						player.setIsPlaying(players.size() <= 1 ? false : isWaitingState());
						player.setTable(getTable());
						player.setServerSeat(seat);
						updatePlayerCount();
						new PlayerJoin(getRoomExtension()).sendSelf(player);
						new PlayerJoin(getRoomExtension()).sendAll();
					}
				} else if (isRoomCreator(user)) // create owner room ,be a dealer
				{
					user.removeProperty("create_room");

					if (player.getUser().isSpectator())
						getRoomExtension().getApi().spectatorToPlayer(player.getUser(),
								getRoomExtension().getGameRoom(), false, true);
					if (!players.contains(player)) {
						players.add(player);
						player.setIsPlaying(players.size() <= 1 ? false : isWaitingState());
						player.setTable(getTable());
						player.setServerSeat(0);
						updatePlayerCount();
						new PlayerJoin(getRoomExtension()).sendSelf(player);
						new PlayerJoin(getRoomExtension()).sendAll();
					}
				}

				new PlayerJoin(getRoomExtension()).sendAllToSelf(user);
			}
		} catch (Exception ex) {
			System.out.println("ERROR join => " + ex.getMessage());
		}
	}

	public void stand(Player player) {
		try {
			if (player.getUser().isPlayer())
				getRoomExtension().getApi().playerToSpectator(player.getUser(), getRoomExtension().getGameRoom(), false,
						true);
			if (players.contains(player))
				this.players.remove(player);
			updatePlayerCount();
			new PlayerJoin(getRoomExtension()).sendAll();
		} catch (Exception e) {
		}
	}

	public void Remove(Player player) {
		try {
			if (players.contains(player))
				this.players.remove(player);
			if (player.getServerSeat() != -1)
				new PlayerJoin(getRoomExtension()).sendAll();
			updatePlayerCount();
		} catch (Exception e) {
		}
	}

	public STATE gameState = STATE.CLEAR;

	public void cleanPlayerWithSFSUser() {

		// add user for remove when next game
		List<Player> tempList = new ArrayList<Player>();
		for (int i = 0; i < players.size(); i++) {
			Player player = players.get(i);
			if (player.getIsRequestLeave()) {
				tempList.add(player);
			} else if (player.getIsRequestStand()) {
				tempList.add(player);
			} else if (player.isNotEnoughBalanceToPlayNext()) {
				tempList.add(player);
			} else if (!isUserExist(player.getId())) {
				tempList.add(player);
			}
		}
		// remove player
		for (int i = 0; i < tempList.size(); i++) {
			Player p = tempList.get(i);
			if (!isUserExist(p.getId())) {
				GameUtil.stopPlay(p.getUser());
				Remove(p);
			} else if (p.getIsRequestLeave()) {
				GameUtil.leaveRoom(p, getRoomExtension());
				Remove(p);
			} else if (p.getIsRequestStand() || p.isNotEnoughBalanceToPlayNext()) {
				new Stand(getRoomExtension()).sendMessage(p, true);
				stand(p);
			}
		}

		waitReconnectUsers.clear();

		int userCount = getRoomExtension().getGameRoom().getUserList().size();
		if (userCount == 0 && getTable().room.isCustomRoom) {
			GameUtil.removeRoom(getRoomExtension());
		}

		if (getRoomExtension().getGameRoom().containsProperty("users")) {
			getRoomExtension().getGameRoom().removeProperty("users");
		}

		if (!getRoomExtension().getGameRoom().containsVariable("state")
				|| !getRoomExtension().getGameRoom().getVariable("state").getStringValue().equals("WAITING")) {
			getRoomExtension().updateRoomVariableStatus(gameState.name());
		}

		////////////////////////////
		for (int i = 0; i < players.size(); i++) {
			Player p = players.get(i);
			p.clear();
			if (gameState == STATE.NEW_GAME)
				new PlayerJoin(getRoomExtension()).sendSelf(p);
		}

		if (gameState == STATE.NEW_GAME) {
			gameState = STATE.WAITING;
			// new PlayerJoin(getRoomExtension()).sendAll();
		}

	}

	public void addReconnectUser(Player player) {
		waitReconnectUsers.add(player.getUser());
		getRoomExtension().getGameRoom().setProperty(Key.KEY_USERS, waitReconnectUsers);
	}

	public List<Player> getPlayers() {
		return this.players;
	}

	public Player getDealer() {
		// Player player = this.getActivePlayers().forEach(p->p.getIsDealer());
		List<Player> players = this.getActivePlayers();
		for (int i = 0; i < players.size(); i++) {

			Player p = players.get(i);
			if (p.getIsDealer()) {
				return p;
			}

		}
		return null;
	}

	public void addPlayer(Player player) {
		this.players.add(player);
	}

	public Player getPlayerById(String id) {
		for (int i = 0; i < getPlayers().size(); i++) {
			Player p = players.get(i);
			if (p.getId().equals(id))
				return p;
		}
		return null;
	}

	public void robotInit(int maxWaitTime) {
		List<Player> activeMembers = getActivePlayers();
		for (int i = 0; i < activeMembers.size(); i++) {
			Player player = activeMembers.get(i);
			if (player.isRobot())
				player.init(maxWaitTime);
		}
	}

	public void robotRanking(int maxWaitTime) {
		List<Player> activeMembers = getActivePlayers();
		for (int i = 0; i < activeMembers.size(); i++) {
			Player player = activeMembers.get(i);
			if (player.isRobot())
				player.ranking(maxWaitTime);
		}
	}

	public void robotConfirm(int maxWaitTime) {
		List<Player> activeMembers = getActivePlayers();
		for (int i = 0; i < activeMembers.size(); i++) {
			Player player = activeMembers.get(i);
			if (player.isRobot())
				player.confirm(maxWaitTime);
		}
	}

	public boolean isWinSpecial() {
		return getActivePlayersWinSpecial().size() > 0;
	}

	public List<Player> getActivePlayersWinSpecial() {
		List<Player> activePlayers = new ArrayList<Player>();
		for (int i = 0; i < getPlayers().size(); i++) {
			Player player = getPlayers().get(i);
			if (player != null && player.getIsPlaying() && player.getIsWinSpecialCombination())
				activePlayers.add(player);
		}
		return activePlayers;
	}

	public List<Player> getSeatedPlayerNotPlayingYet() {
		List<Player> seatedNotPlaying = new ArrayList<Player>();
		for (int i = 0; i < getPlayers().size(); i++) {
			Player player = getPlayers().get(i);
			if (player != null && player.getServerSeat() != -1 && !player.getIsPlaying())
				seatedNotPlaying.add(player);
		}
		return seatedNotPlaying;
	}

	int startIndex = 0;

	public List<Player> getSortedPlayerTurn() {
		List<Player> sortedPlayers = new ArrayList<Player>();
		List<Player> playerOrder = getActivePlayers().stream().sorted(Comparator.naturalOrder())
				.collect(Collectors.toList());

		startIndex = getDealer().getServerSeat();
		for (int i = 0; i < room.maxPlayer; i++) {

			playerOrder.forEach((p) -> {		

				if (p.getServerSeat() == startIndex) {
					sortedPlayers.add(p);				
				}			
			});
			startIndex++;
			if (startIndex == room.maxPlayer) {
				startIndex = 0;
			}
		}


		return sortedPlayers;
	}
	
//	public List<Player> getSortedPlayerTurn() {
//		  List<Player> sortedPlayers = new ArrayList<>();
//		  List<Player> playerOrder = getActivePlayers().stream().sorted(Comparator.naturalOrder())
//		      .collect(Collectors.toList());
//
//		  int startSeat = getDealer().getServerSeat();
//		  for (int i = 0; i < room.maxPlayer; i++) {
//		    sortedPlayers.add(playerOrder.get(startSeat));
//		    startSeat++;
//		    if (startSeat == room.maxPlayer) {
//		      startSeat = 0;
//		    }
//		  }
//
//		  return sortedPlayers;
//		}



	public List<Player> getActivePlayers() {
		// players = GameUtil.removeDuplicates(players);
		List<Player> activePlayers = new ArrayList<Player>();
		for (int i = 0; i < getPlayers().size(); i++) {
			Player player = getPlayers().get(i);
			if (player != null && player.getIsPlaying()) {
				activePlayers.add(player);
			}

		}
		return activePlayers;
	}

	public List<String> getActiveProvider() {
		List<String> activeProvider = new ArrayList<String>();
		List<Player> activePlayers = getActivePlayers();
		for (int i = 0; i < activePlayers.size(); i++) {
			Player player = activePlayers.get(i);
			if (!activeProvider.contains(player.getPlayerInfo().getProviderName()))
				activeProvider.add(player.getPlayerInfo().getProviderName());
		}
		return activeProvider;
	}

	public void robotBuyJackpot() {
		Random rnd = new Random();
		for (int i = 0; i < getActivePlayers().size(); i++) {
			Player player = getPlayers().get(i);
			int jackpotId = rnd.nextInt(5) + 1;
			if (player.isRobot()) {
				player.buyJackpotNextRound(jackpotId);
			}
		}
	}

	public void buyJackpot() {
		for (int i = 0; i < getActivePlayers().size(); i++) {
			Player player = getPlayers().get(i);
			player.currentJackpotData = new JackpotData(player.nextJackpotData.jackpotId,
					player.nextJackpotData.jackpotAmount, player.nextJackpotData.jackpotId != -1);
			player.buyJackpot();
		}
	}

	public ISFSArray toSFSObjectDealCard(Integer seat) {
		SFSArray params = new SFSArray();
		//List<Player> activePlayers = getActivePlayers();
		List<Player> activePlayers = getSortedPlayerTurn();
		for (int i = 0; i < activePlayers.size(); i++) {
			Player player = activePlayers.get(i);
			params.addSFSObject(player.toSFSObjectDealCard(seat));
		}
		return params;
	}

	public ISFSArray toSFSObjectDealCardAll() {
		SFSArray params = new SFSArray();
		List<Player> activePlayers = getActivePlayers();
		for (int i = 0; i < activePlayers.size(); i++) {
			Player player = activePlayers.get(i);
			params.addSFSObject(player.toSFSObjectHideCard());
		}
		return params;
	}

	public void clear() {
		List<Player> memebers = getActivePlayers();
		for (int i = 0; i < memebers.size(); i++) {
			Player player = memebers.get(i);
			player.clear();
		}
	}

	public void updatePlayerCount() {
		ArrayList<RoomVariable> rv = new ArrayList<RoomVariable>();
		rv.add(new SFSRoomVariable("player_count", getSeateds().size(), false, true, true));
		getRoomExtension().getApi().setRoomVariables(null, getRoomExtension().getGameRoom(), rv, true, true, false);
	}

	private GameTemplate getGameTemplate() {
		return getRoomExtension().getGameTemplate();
	}

	private Table getTable() {
		return getGameTemplate().getGame().getTable();
	}

	public List<Integer> getSeateds() {
		List<Integer> seated = new ArrayList<Integer>();
		for (int p = 0; p < players.size(); p++) {
			Player player = players.get(p);
			if (player.getServerSeat() >= 0) {
				if (!seated.contains(player.getServerSeat()))
					seated.add(player.getServerSeat());
			}
		}
		return seated;
	}

	private boolean isWaitingState() {
		if (getGameTemplate().getState() == STATE.WAITING || getActivePlayers().size() < room.minPlayer)
			return true;
		return false;
	}

	private boolean isPlayerExist(String id) {
		List<Player> members = getPlayers();
		for (int i = 0; i < members.size(); i++) {
			Player member = members.get(i);
			if (member.getId().equals(id))
				return true;
		}
		return false;
	}

	private Boolean isUserExist(String id) {
		List<User> users = getRoomExtension().getGameRoom().getUserList();
		for (int i = 0; i < users.size(); i++) {
			User user = users.get(i);
			PlayerInfo playerInfo = GameUtil.getPlayerInfo(user);
			if (playerInfo.getId().equals(id))
				return true;
		}
		return false;
	}

	private boolean isRoomCreator(User user) {
		return user.containsProperty("create_room");
	}

	public RoomExtension getRoomExtension() {
		return this.roomExtensionAtomic.get();
	}

	public boolean isEnoughPlayer() {
		long activeCount = getPlayers().stream().filter(p -> p.getServerSeat() >= 0).count();
		if (activeCount >= room.minPlayer) {
			return true;
		}

		return false;
	}

//	public void setDealer() {
//		Stream<Player> player = getPlayers().stream().filter(p -> p.getServerSeat() >= 0);
//		if (player.count() <= minPlayer) {
//			player.forEach(p -> {
//				p.setIsDealer(true);
//				
//				System.out.println("dealer ::: " + p.getName());
//			});
//		}
//	}

	public ISFSArray toSFSArrayActivePlayers() {
		SFSArray params = new SFSArray();
		List<Player> player_seats = getTable().seatManager.getSeatedPlayers();
		for (int i = 0; i < player_seats.size(); i++) {
			Player player = player_seats.get(i);
			params.addSFSObject(player.toSFSObjectAllPlayer());
		}
		return params;
	}

//	public ISFSArray toSFSArrayPlayersBetting()
//	{
//		SFSArray params = new SFSArray();
//		List<Player> player_seats = getTable().seatManager.getSeatedPlayers();
//		for(int i = 0;i<player_seats.size();i++)
//		{
//			Player player = player_seats.get(i);
//			params.addSFSObject(player.toSFSObjectAllPlayerBetting());
//		}
//		return params;
//	}

	public ISFSArray toSFSObjectResult(String handName) {
		SFSArray params = new SFSArray();
		return params;
	}

	public static <T extends Comparable<T>> void sortList(List<T> list, int fromIndex, int toIndex) {
		Collections.sort(list.subList(fromIndex, toIndex));
	}
}
