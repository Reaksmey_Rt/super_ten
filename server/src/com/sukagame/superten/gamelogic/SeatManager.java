package com.sukagame.superten.gamelogic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

//import com.sukagame.superten.roomclientrequestevent.BaseSukaRoomClientRequestHandler;
//import com.sukagame.superten.send.PlayerJoin;
//import com.sukagame.superten.send.Seat;
////extends BaseSukaRoomClientRequestHandler
public class SeatManager {

	private PlayerController p_controller ;
	public int seatId = -1;
	
	public  SeatManager(PlayerController p_controller) {
		this.p_controller = p_controller;
	}
	
	public boolean isSeatAvailable(int seatId)
	{
		if(getAvailableSeats().contains(seatId))
			return true;
		return false;
	}
	
	Integer getSeat()
	{
		List<Integer> availableSeats = getAvailableSeats();
		Random random = new Random();
		if(availableSeats.size() == 0)
			return -1;
		return availableSeats.get(random.nextInt(availableSeats.size()));
	}
	
	private List<Integer> getSeateds()
	{
		List<Integer> seated = new ArrayList<Integer>();
		for(int p = 0; p < p_controller.getActivePlayers().size(); p++)
		{
			
			Player player =  p_controller.getActivePlayers().get(p);
			if(player.getServerSeat() >= 0 )
			{
				if(!seated.contains(player.getServerSeat()))
					seated.add(player.getServerSeat());
			}
		}
		return seated;
	}
	
	
	List<Integer> getAvailableSeats()
	{
		List<Integer> allSeats = Arrays.asList(0,1,2,4,3,5,6,7);
		List<Integer> seateds  = getSeateds();
		List<Integer> availableSeats = allSeats.stream()
				.filter(seat -> !seateds.contains(seat))
				.collect(Collectors.toList());
		return availableSeats;
	}
	

	public List<Player> getSeatedPlayers()
	{
		List<Player> activePlayers = new ArrayList<Player>();
		for(int i = 0; i <p_controller.getPlayers().size();i++)
		{
			Player player = p_controller.getPlayers().get(i);
			if(player != null && player.getServerSeat() !=-1)
				activePlayers.add(player);
		}
		return activePlayers;
	}
	
	
		 
}
