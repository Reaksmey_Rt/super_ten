package com.sukagame.superten.gamelogic;

import com.sukagame.superten.room.RoomExtension;
import com.sukagame.superten.util.GameUtil;

public class GameTemplate {
	public enum STATE {
		WAITING, JACKPORT, NEW_GAME, COLLECT_CHIP, SHUFFLE_DECK, DEAL_2_CARD, REVEAL_CARD, OPEN_CARD, BETTING,
		DEAL_1_CARD, RESULT, CLEAR;

	}

	private STATE state = STATE.WAITING;
	private Game game;
	private RoomExtension roomExtension;

	public GameTemplate(RoomExtension roomExtension) {
		state = STATE.WAITING;
		this.roomExtension = roomExtension;
		game = new Game(this);
	}

	public void setState(STATE state) {
		this.state = state;
	}

	public STATE getState() {
		return this.state;
	}

	public String getStateName() {
		return this.state.name();
	}

	public void Play() {
		this.setState(STATE.NEW_GAME);
		while (state != STATE.WAITING) {
			getRoomExtension().updateRoomVariableStatus(getStateName());

			switch (state) {
			case WAITING:
				game.Waiting();
				break;
			case NEW_GAME:
				game.newGame();
				break;
			case COLLECT_CHIP:
				game.collectChip();
				break;
			case SHUFFLE_DECK:
				game.shuffleDeck();
				break;
			case DEAL_2_CARD:
				game.dealCard(2);
				break;
			case REVEAL_CARD:
				game.revealCard();
				break;
			case OPEN_CARD:
				game.openCard();
				break;
			case BETTING:
				game.bettingTurn();
				break;
			case DEAL_1_CARD:
				game.dealCard(1);
				break;

			case RESULT:
				game.Result();
				break;
			case CLEAR:
				game.Stop();
				break;
			default:
				this.Stop();
				break;
			}
			// System.out.println("State : "+ state);
		}
		// Clear();
	}

	private int miliMax = 5000;
	private int mili = 0;

	public void Remove() {
		mili += 100;
		if (mili >= miliMax) {
			GameUtil.removeRoom((RoomExtension) game.getTable().roomExtension.getGameRoom());
			game.getTable().roomExtension.destroy();
			setState(GameTemplate.STATE.CLEAR);
			mili = 0;
		}
	}

	public void Clear() {
		game.Waiting();
	}

	public void Stop() {
		this.setState(STATE.CLEAR);
	}

	public Game getGame() {
		return this.game;
	}

	public RoomExtension getRoomExtension() {
		return this.roomExtension;
	}
}
