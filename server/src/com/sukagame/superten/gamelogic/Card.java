package com.sukagame.superten.gamelogic;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.sukagame.global.Key;
import com.sukagame.superten.database.model.Room;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public final class Card implements Comparable<Card>, Cloneable {
	private int value;
	private int oValue;
	private Suit suit;


	public static List<Card> createOrderedDeck(Room room) {
		List<Card> cards = new ArrayList<>();

		for (int value = 1; value <= 13; value++) {
			for (Suit suit : Suit.values()) 
			{
				int orderValue = value;
				if(orderValue == 1)
					orderValue = 14;
				Card card = new Card(value, suit,orderValue);
				cards.add(card);
			}
		}
		return cards;
	}

	public static void shuffleCards(List<Card> cards) {
		Random random = new Random();
		for (int i = cards.size() - 1; i > 0; i--) {
			int index = random.nextInt(i + 1);
			Card card = cards.get(index);
			cards.set(index, cards.get(i));
			cards.set(i, card);
		}
	}

	public static List<Card> createShuffledDeck(Room room) {
		List<Card> deck = createOrderedDeck(room);
		int shuffleCount = new Random().nextInt(500) + 40;
		for (int i = 0; i < shuffleCount; i++)
			shuffleCards(deck);
		return deck;
	}

	public Card(int value, Suit suit) {
		this.value = value;
		this.suit = suit;
	}

	public Card(int value, Suit suit,int oValue) {
		this.value = value;
		this.suit = suit;
		this.oValue = oValue;
	}

	public boolean isJack() {
		return value == 11;
	}

	public boolean isQueen() {
		return value == 12;
	}

	public boolean isKing() {
		return value == 13;
	}

	public boolean isAce() {
		return value == 14 || value == 1;
	}

	public String getName() {
		if (isAce())
			return "A";
		if (isKing())
			return "K";
		if (isQueen())
			return "Q";
		if (isJack())
			return "J";
		else
			return "" + value;
	}

	public int getOrderSpecialValue()
	{
		if(oValue == 14)
			return 1;
		return oValue;
	}

	public int getOrderValue()
	{
		return this.oValue;
	}

 	public Suit getSuit() {
		return suit;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Card card = (Card) o;

		return value == card.value && suit == card.suit;
	}

	@Override
	public int hashCode() {
		int result = value;
		result = 31 * result + suit.hashCode();
		return result;
	}

	@Override
	public String toString() {
		return getName() + "" + getSuit();
	}

	public ISFSObject toSFSObject()
	{
		ISFSObject param = new SFSObject();
		param.putUtfString(Key.KEY_ROOM_CARD_SUIT, suit.toString());
		param.putUtfString(Key.KEY_ROOM_CARD_VALUE, getName());
		return param;
	}

	@Override
	public int compareTo(Card o) {
		if (o.getOrderValue() == getOrderValue())
		{
			return  suit.getSuitValue() -o.getSuit().getSuitValue();
		}
		return  getOrderValue() - o.getOrderValue();
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		Object obj = super.clone();
		if (obj instanceof Card) {
			((Card)obj).value = value;
			((Card)obj).suit = suit;
		}
		return obj;
	}

	public enum Suit {

		SPADES(4),
		HEARTS(3),
		CLUBS(2),
		DIAMONDS(1);

		int value;

		Suit(int value) {
			this.value = value;
		}

		public int getSuitValue() {
			return value;
		}

		public ISFSObject toSFSObject()
		{
			ISFSObject param = new SFSObject();
			param.putInt(Key.KEY_ROOM_CARD_SUIT, value);
			return param;
		}

		@Override
		public String toString() {
			return super.name().substring(0, 1);
		}
	}
}
