package com.sukagame.superten.gamelogic;

import com.sukagame.global.Key;

import com.sukagame.superten.util.GameUtil;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Hand {
	private List<Card> cards = new ArrayList<>();
	public Player player;
	private int dealCardAmt = 0;

	public Hand(Player player) {
		this.player = player;
	}

	public Player getPlayer() {
		return this.player;
	}

	public List<Card> getCards() {
		return this.cards;
	}

	public boolean addCardToHand(Card newCard) {
		return this.cards.add(newCard);
	}

	public void sort() {
		Collections.sort(cards);
	}

	public ISFSObject toSFSObjectDealCard() {
		ISFSObject param = new SFSObject();
		param.putInt(Key.KEY_ROOM_CARD_COUNT, getDealCardsAmount(getCards().size()));
		//param.putInt(Key.KEY_ROOM_CARD_COUNT, cards.size());
		param.putSFSArray(Key.KEY_ROOM_PLAYER_CARDS, GameUtil.toSFSObject(cards));
		return param;
	}

	public ISFSObject sentSFSObjectCards() {
		ISFSObject param = new SFSObject();
		param.putInt(Key.KEY_ROOM_CARD_COUNT, getDealCardsAmount(getCards().size()));
		//param.putInt(Key.KEY_ROOM_CARD_COUNT, cards.size());
		param.putSFSArray(Key.KEY_ROOM_PLAYER_CARDS, GameUtil.toSFSObject(cards));
		return param;
	}

	public ISFSObject toSFSObjectHideCard() {
		ISFSObject param = new SFSObject();
		// System.out.println("card :::::: " +cards.size()+"  "+getPlayer().getName());
		param.putInt(Key.KEY_ROOM_CARD_COUNT, getDealCardsAmount(cards.size()));
		//param.putInt(Key.KEY_ROOM_CARD_COUNT, cards.size());
		param.putSFSArray(Key.KEY_ROOM_PLAYER_CARDS, new SFSArray());
		return param;
	}

	public ISFSObject toSFSObjectShowSpecialCard() {
		ISFSObject param = new SFSObject();
		param.putInt(Key.KEY_ROOM_CARD_COUNT, cards.size());
		param.putSFSArray(Key.KEY_ROOM_PLAYER_CARDS, GameUtil.toSFSObject(cards));
		return param;
	}

	public int getDealCardsAmount(int cardsAmt) {

		dealCardAmt = (cardsAmt > 2) ? 1 : cardsAmt;
		return dealCardAmt;
	}

	public void clear() {
		cards.clear();
	}
}
