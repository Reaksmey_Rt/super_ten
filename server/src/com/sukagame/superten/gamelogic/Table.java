package com.sukagame.superten.gamelogic;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.smartfoxserver.v2.util.stats.CCULoggerTask;
import com.sukagame.global.RoomCommand;
//import com.sukagame.global.GameManager;
import com.sukagame.superten.database.GameDatabaseManager;
import com.sukagame.superten.database.SukaDatabaseManager;
import com.sukagame.superten.database.model.PlayerInfo;
import com.sukagame.superten.database.model.Room;
import com.sukagame.superten.room.RoomExtension;
import com.sukagame.superten.send.*;
import com.sukagame.superten.util.GameUtil;
import com.sukagame.superten.util.Util;

public class Table {
	public String roomId = "";
	public String gamePlayId = "";
	public String gamePlayCode = "";
	public String gameId = "G03";
	private int activePlayerCount = 0;
	public int roundCount = 0;
	public int gameCount = 0;
	public double fee = 0;
	public double bet_amount = 0;

	public Room room;
	public boolean isSpecialWin = false;
	public boolean isWalletSeamless = false;
	public PlayerController playerController;
	public SeatManager seatManager;
	public Deck deck;
	public RoomExtension roomExtension;
	public GameTemplate gameTemplate;

	public Table(GameTemplate gameTemplate) {
		this.roomExtension = gameTemplate.getRoomExtension();
		this.gameTemplate = gameTemplate;
		this.playerController = roomExtension.getPlayerController();
		this.seatManager = new SeatManager(playerController);
		room = GameUtil.getRoom(roomExtension.getGameRoom());
		deck = new Deck(room);
		fee = new SukaDatabaseManager().getFee();
	}

	public PlayerController getPlayerController() {
		return this.playerController;
	}

	public void waiting() {
		// playerController.cleanPlayerWithSFSUser();

	}

	public void newGame() {
		// playerController.gameState = GameTemplate.STATE.NEW_GAME;
		roundCount = 0;
		gameCount++;
		isSpecialWin = false;

		roomId = GameUtil.getRoomId(roomExtension);
		gamePlayId = Util.getGamePlayId(roomId, gameCount);
		gamePlayCode = Util.getGamePlayCode(gameCount);
		playerController.getSeatedPlayerNotPlayingYet().forEach(p -> {
			p.setIsPlaying(true);
		});

		// playerController.clear();
		activePlayerCount = playerController.getSortedPlayerTurn().size();
		roomExtension.updateRoomVariable(gamePlayId, gamePlayCode);
		new NewGame(roomExtension).send(activePlayerCount, roundCount, gameCount);
		// new GameDatabaseManager().AddGamePlay(gamePlayId, gamePlayCode, roomId);

		// playerController.setDealer();

	}

	public void collectChip() {
		// playerController.gameState = GameTemplate.STATE.COLLECT_CHIP;

		List<Player> seats = playerController.getSortedPlayerTurn();
		for (int p = 0; p < seats.size(); p++) {
			Player player = seats.get(p);
			player.setBetting(room.betMinAmout);
		}

		new CollectChips(roomExtension).sendAllPlayersBetting();

	}

	public void jackpot() {
		playerController.robotBuyJackpot();
		playerController.buyJackpot();
	}

//	public void newRound() {
//		roundCount++;
//		new NewRound(roomExtension).send(roundCount);
//	}

	public void shuffleDeck() {
		// playerController.gameState = GameTemplate.STATE.SHUFFLE_DECK;
		deck.shuffle();
		new General(roomExtension).sendUpdateState(RoomCommand.CMD_SHUFFLE_DECK);
	}

	public void dealCard(int cardAmt) {
		if (cardAmt == 2) {
			playerController.gameState = GameTemplate.STATE.DEAL_2_CARD;
		} else {
			playerController.gameState = GameTemplate.STATE.DEAL_1_CARD;
		}

		playerController.getSortedPlayerTurn().forEach(p -> { // Add cards to players

			for (int i = 0; i < cardAmt; i++) {
				p.addCard(deck.dealCard());
			}
		});

		playerController.getSortedPlayerTurn().forEach(p -> {
			System.out.println("seat : " + p.getServerSeat());
			new CardSFSObject(roomExtension).sendDealCardByPlayer(p);
		});

	}

	public void revealCard(int cardAmt) {

		new CardSFSObject(roomExtension).sendRevealCardByPlayer(cardAmt);
	}



	public void RunTurn() {

	
		playerController.getSortedPlayerTurn().forEach(p -> {
	
			new BettingTurn(roomExtension).sendAllPlayersTurn(p);
			GameUtil.wait(10, roomExtension);
		});


	}



	public void gameResult() {
		new General(roomExtension).sendUpdateState(RoomCommand.CMD_RESULT);
		// playerController.toSFSObjectResult(gameId);
	}

	public void stop() {
		new General(roomExtension).sendUpdateState(RoomCommand.CMD_CLEAR);
		playerController.clear();
		gameCount = 0;
		roundCount = 0;
	}

	public void openCard(int cardAmt) {
		new CardSFSObject(roomExtension).sendOpenCardByPlayer(cardAmt);
		// new General(roomExtension).sendUpdateState(RoomCommand.CMD_OPEN_CARD);

	}

}
