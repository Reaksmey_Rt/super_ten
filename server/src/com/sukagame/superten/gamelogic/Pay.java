package com.sukagame.superten.gamelogic;
//
//import com.sukagame.superten.gamelogic.Player;
//import com.sukagame.superten.gamelogic.Table;

public class Pay
{
	private double remainAmount;
	private double commission;
	private double shareCapital;
	
	private Table table;
	public double totalRewarded;

	public Pay(Table table,Player player) {
		this.table =table;
	}
	
	public double process(double amount)
	{
		remainAmount = remainAmount + amount;
		return remainAmount;
	}
	
	public double getRemainAmount()
	{
		return this.remainAmount;
	}
	
	public double getFee()
	{
		return remainAmount > 0 ? remainAmount * table.fee : 0;
	}
	
	public void setCommission(double commission)
	{
		this.commission += commission;
	}
	
	public double getCommission()
	{
		return this.commission;
	}
	
	public void setShareCapital(double commission)
	{
		this.shareCapital += commission;
	}
	
 	public double getShareCapital()
	{
		return this.shareCapital;
	}
	
	public double getFinalAmount()
	{
		return remainAmount - getFee();
	}
}
