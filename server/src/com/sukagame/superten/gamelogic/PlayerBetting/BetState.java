package com.sukagame.superten.gamelogic.PlayerBetting;

public class BetState {

	  public enum BetTypes {
	    NONE,
	    CHECK,
	    ALL_IN,
	    CALL,
	    BET,
	    RAISE,
	    FOLD;
	  }

	  private BetTypes betType;

	  public BetState() {
	    this.betType = BetTypes.NONE;
	  }

	  public BetTypes getBetType() {
	    return betType;
	  }

	  public void setBetType(BetTypes betType) {
	    this.betType = betType;
	  }

	  public void changeBetType(BetTypes newBetType) {
	    this.betType = newBetType;
	  }

	}