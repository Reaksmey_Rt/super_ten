package com.sukagame.superten.gamelogic.PlayerBetting;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class BetSummary
{
	public int betTypeId;
	public String betType;
	public double totalBet;
	public double totalRewarded;

	public BetSummary(Integer betTypeId,double totalBet,double totalRewarded)
	{
		this.betTypeId = betTypeId;
		this.totalBet = totalBet;
		this.totalRewarded = totalRewarded;
	}

	public BetSummary(Integer betTypeId,String betType, double totalBet,double totalRewarded)
	{
		this.betTypeId = betTypeId;
		this.betType = betType;
		this.totalBet = totalBet;
		this.totalRewarded = totalRewarded;
	}

	public ISFSObject toSFSObject()
	{
		ISFSObject param = new SFSObject();
		param.putInt("bet_type_id", betTypeId);
		param.putDouble("total_bet", totalBet);
		param.putDouble("total_rewarded", totalRewarded);
		return param;
	}

	public ISFSObject toSFSObjectDetail()
	{
		ISFSObject param = new SFSObject();
		param.putInt("bet_type_id", betTypeId);
		param.putUtfString("bet_type", betType);
		param.putDouble("total_bet", totalBet);
		param.putDouble("total_rewarded", totalRewarded);
		return param;
	}
}
