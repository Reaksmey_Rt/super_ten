package com.sukagame.superten.gamelogic.PlayerBetting;

import com.sukagame.superten.gamelogic.PlayerBetting.BetState.BetTypes;

public class Bet {

	public double betAmount;

	public BetTypes value;

	public Bet(BetTypes value) {
		this.value = value;
	}

}
