package com.sukagame.superten.gamelogic;

import com.sukagame.superten.database.model.Room;

public class HumanPlayer extends Player
{

	public HumanPlayer(PlayerController playerController,Room room)
	{
		this.playerController = playerController;
		this.roomExtension = playerController.getRoomExtension();
		this.room = room;
	}
	
	@Override
	public void init(int maxTime)
	{
		
	}

	@Override
	public void ranking(int currentTime)
	{
		
	}

	@Override
	public void confirm(int currentTime)
	{
		
	}

}
