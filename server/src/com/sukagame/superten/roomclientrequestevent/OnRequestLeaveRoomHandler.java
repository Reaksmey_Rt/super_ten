package com.sukagame.superten.roomclientrequestevent;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.sukagame.superten.gamelogic.GameTemplate;
import com.sukagame.superten.gamelogic.Player;
import com.sukagame.superten.gamelogic.GameTemplate.STATE;
import com.sukagame.superten.send.General;
import com.sukagame.superten.util.GameUtil;

public class OnRequestLeaveRoomHandler extends BaseSukaRoomClientRequestHandler
{
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		super.handleClientRequest(user, params);
		
		String id = GameUtil.getPlayerInfo(user).getId();
		Player player = getPlayerController().getPlayerById(id);
		if(player != null)
		{
			if(isCanRemove(player))
			{
				GameUtil.leaveRoom(user,roomExtension);
				roomExtension.getPlayerController().Remove(player);
			}
			else
			{
				player.setIsRequestLeave(!player.getIsRequestLeave());
				new General(roomExtension).SendRespondLeavePending(player);
			}
		}
		else
		{
			GameUtil.leaveRoom(user,roomExtension);
		}
	}

	private boolean isCanRemove(Player player)
	{
		if(gameTemplate().getState() == STATE.WAITING || player.getIsPlaying() == false)
			return true;
		return false;
	}
	
	private GameTemplate gameTemplate()
	{
		return roomExtension.getGameTemplate();
	}
}
