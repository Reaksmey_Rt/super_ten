package com.sukagame.superten.roomclientrequestevent;

//import java.awt.Label;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
//import com.smartfoxserver.v2.entities.data.SFSObject;
import com.sukagame.global.GameManager;
import com.sukagame.superten.database.model.PlayerInfo;
import com.sukagame.superten.database.model.Room;
import com.sukagame.superten.gamelogic.Player;
//import com.sukagame.superten.gamelogic.SeatManager;
//import com.sukagame.superten.gamelogic.Table;
import com.sukagame.superten.send.BuyIn;
import com.sukagame.superten.send.PlayerJoin;
import com.sukagame.superten.send.Seat;
import com.sukagame.superten.util.GameUtil;

public class OnRequestSitHandler extends BaseSukaRoomClientRequestHandler {
	int seatId = -1;
	PlayerInfo playerInfo;
	Player player;

	@Override
	public void handleClientRequest(User user, ISFSObject params) {
		super.handleClientRequest(user, params);
		seatId = params.getInt("chair_id");	
		playerInfo = GameUtil.getPlayerInfo(user);
		Room room = GameUtil.getRoom(roomExtension.getGameRoom());		
		player = getPlayerController().getPlayerById(playerInfo.getId());
		player.setRequestSeat(seatId);
		String smsString = "";
		Boolean isCanBuyIn = true;

		if (player == null) {
			isCanBuyIn = false;
			// return;
		} else if (isAlreadyHaveASeat(playerInfo)) {
			isCanBuyIn = false;
			smsString = "You Already Seat !";
			// return;
		}

		else if (playerInfo.getBalance() < getTable().room.minBalance) {
			smsString = "Your Balance Not Enough !";
			isCanBuyIn = false;
			// return;
		}

		// seat full
		else if (getPlayerController().getActivePlayers().size() >= GameManager.getInstance().maxPlayer) {
			smsString = "Seat Full!";
			isCanBuyIn = false;
			// return;
		}

		// seat not available
		else if (!getTable().seatManager.isSeatAvailable(seatId)) {
			smsString = "seat not available !";
			isCanBuyIn = false;
			// return;
		}

		// seat wrong index
		else if (seatId < 0 || seatId > GameManager.getInstance().maxPlayer - 1) {
			// smsString = "Your Not Enough !";
			isCanBuyIn = false;

		}

		if (!isCanBuyIn) {
			new Seat(roomExtension).sendMessage(player, smsString);
			return;
		}
		
		
		
		new BuyIn(roomExtension).SendBuyIn(player, room, isCanBuyIn);
		// send information to client

//		else if (isGamePlaying()) {
//			if (player.getUser().isSpectator()) {
//				try {
//					roomExtension.getApi().spectatorToPlayer(player.getUser(), roomExtension.getGameRoom(), false,
//							true);
//				} catch (Exception ex) {
//					System.out.println("ERROR handleClientRequest => " + ex.getMessage());
//				}
//			}
//
//			if (!getPlayerController().getPlayers().contains(player)) {
//				getPlayerController().addPlayer(player);
//				player.setIsPlaying(false);
//				player.setTable(getTable());
//				player.setServerSeat(seatId);
//				getPlayerController().updatePlayerCount();
//				new PlayerJoin(roomExtension).sendSelf(player);
//				new PlayerJoin(roomExtension).sendAll();
//				new Seat(roomExtension).sendMessage(player, "wait for next game.");
//			}
//		} else {
//
//			if (player.getUser().isSpectator()) {
//				try {
//					roomExtension.getApi().spectatorToPlayer(player.getUser(), roomExtension.getGameRoom(), false,
//							true);
//				} catch (Exception ex) {
//					System.out.println("ERROR handleClientRequest => " + ex.getMessage());
//				}
//			}
//
//			if (!getPlayerController().getPlayers().contains(player)) {
//				getPlayerController().addPlayer(player);
//				player.setIsPlaying(true);
//				player.setTable(getTable());
//				player.setServerSeat(seatId);
//				getPlayerController().updatePlayerCount();
//				new PlayerJoin(roomExtension).sendSelf(player);
//				new PlayerJoin(roomExtension).sendAll();
//			}
//		}

	}

//	void AllowTosit(Player player, int seatId) {
//
////		  if(isGamePlaying()) { if(player.getUser().isSpectator()) { try {
////		  roomExtension.getApi().spectatorToPlayer(player.getUser(),
////		 roomExtension.getGameRoom(), false, true); }catch (Exception ex) {
////		  System.out.println("ERROR handleClientRequest => " + ex.getMessage()); } }
////		 
////		  if(!getPlayerController().getPlayers().contains(player)) {
////		  getPlayerController().addPlayer(player); player.setIsPlaying(false);
////		  player.setTable(getTable()); player.setServerSeat(seatId);
////		  getPlayerController().updatePlayerCount(); new
////		  PlayerJoin(roomExtension).sendSelf(player); new
////		 PlayerJoin(roomExtension).sendAll(); new
////		 Seat(roomExtension).sendMessage(player,"wait for next game."); } } else {
////		 if(player.getUser().isSpectator()) { try {
////		  roomExtension.getApi().spectatorToPlayer(player.getUser(),
////		  roomExtension.getGameRoom(), false, true); }catch (Exception ex) {
////		 System.out.println("ERROR handleClientRequest => " + ex.getMessage()); } }
//
//		if (!getPlayerController().getPlayers().contains(player)) {
//			getPlayerController().addPlayer(player);
//			player.setIsPlaying(true);
//			player.setTable(getTable());
//			player.setServerSeat(seatId);
//			getPlayerController().updatePlayerCount();
//			new PlayerJoin(roomExtension).sendSelf(player);
//			new PlayerJoin(roomExtension).sendAll();
//		}
//
//	}

	private boolean isAlreadyHaveASeat(PlayerInfo playerInfo) {
		Player player = getPlayerController().getPlayerById(playerInfo.getId());
		if (player != null && player.getServerSeat() != -1)
			return true;
		return false;
	}
}
