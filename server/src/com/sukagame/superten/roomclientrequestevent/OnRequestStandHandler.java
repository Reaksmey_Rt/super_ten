package com.sukagame.superten.roomclientrequestevent;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.sukagame.superten.database.model.PlayerInfo;
import com.sukagame.superten.gamelogic.Player;
import com.sukagame.superten.send.Stand;
import com.sukagame.superten.util.GameUtil;

public class OnRequestStandHandler extends BaseSukaRoomClientRequestHandler
{
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		super.handleClientRequest(user, params);
		PlayerInfo playerInfo = GameUtil.getPlayerInfo(user);
		if(playerInfo != null)
		{
			Player player = getPlayer(playerInfo.getId());
			if(player == null)
				return;
			if(!isGamePlaying() || !player.getIsPlaying())
			{
				player.setIsPlaying(false);
				getPlayerController().stand(player);
			}
			else
			{
				player.setIsRequestStand(!player.getIsRequestStand());
				new Stand(roomExtension).sendMessage(player,false);
			}
		}
	}
}
