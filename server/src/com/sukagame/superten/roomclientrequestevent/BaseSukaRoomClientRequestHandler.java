package com.sukagame.superten.roomclientrequestevent;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.sukagame.superten.gamelogic.GameTemplate;
import com.sukagame.superten.gamelogic.Player;
import com.sukagame.superten.gamelogic.PlayerController;
import com.sukagame.superten.gamelogic.Table;
import com.sukagame.superten.room.RoomExtension;
import com.sukagame.superten.util.GameUtil;

public class BaseSukaRoomClientRequestHandler extends BaseClientRequestHandler
{
	public RoomExtension roomExtension = null;
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		roomExtension = (RoomExtension)getParentExtension();
	}

	public GameTemplate getGameTemplate()
	{
		return roomExtension.getGameTemplate();
	}
	
	public PlayerController getPlayerController()
	{
		return roomExtension.getPlayerController();
	}
	
	public String getPlayerId(User user)
	{
		return GameUtil.getPlayerInfo(user).getId();
	}
	
	public Player getPlayer(String id)
	{
		return roomExtension.getPlayerController().getPlayerById(id);
	}

	public Table getTable()
	{
		return getGameTemplate().getGame().getTable();
	}
	
	public boolean isGamePlaying()
	{
		return GameUtil.isGamePlaying(getGameTemplate());
	}
}
