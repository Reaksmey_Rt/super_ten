package com.sukagame.superten.roomclientrequestevent;
//
//import java.util.List;
//
//import com.smartfoxserver.v2.core.ISFSEvent;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
//import com.smartfoxserver.v2.exceptions.SFSException;
//import com.sukagame.global.GameManager;
//import com.sukagame.superten.database.GameDatabaseManager;
//import com.sukagame.superten.database.GameDatabaseManager.STORE_PROCEDURE;
//import com.sukagame.superten.database.model.Room;
import com.sukagame.superten.gamelogic.Player;
import com.sukagame.superten.send.Emoji;
//import com.sukagame.superten.util.GameRoom;
//import com.sukagame.superten.util.Util;

public class OnRequestSendEmojiHandler extends BaseSukaRoomClientRequestHandler
{
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		super.handleClientRequest(user, params);
		Player player = this.getPlayer(getPlayerId(user));
		if(player == null)
			return;
		int id = params.getInt("id");
		new Emoji(roomExtension).send(player,id);
	}

//	@Override
//	public void handleServerEvent(ISFSEvent event) throws SFSException
//	{
//		super.handleServerEvent(event);
//		List<Room> rooms = new GameDatabaseManager().getRooms();
//		GameManager.getInstance().roomsDatabase = rooms;
//		for (Room room : rooms)
//		{
//			int noId = new GameDatabaseManager().getLatestId(STORE_PROCEDURE.GET_ROOM_LATEST_ID);
//			room.id = Util.getFormatNumber("R", noId);
//			room.jackpotLimits = new GameDatabaseManager().getJackpotLimits(room.roomTypeId);
//			GameRoom.create(room, GameManager.getInstance().api,GameManager.getInstance().zone, user);
//			return;
//		}
//	}
}
