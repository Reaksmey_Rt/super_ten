package com.sukagame.superten.roomclientrequestevent;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.sukagame.superten.gamelogic.Player;

public class OnRequestBuyJackpotHandler extends BaseSukaRoomClientRequestHandler
{
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		super.handleClientRequest(user, params);

		int jackpotId = params.getInt("id");

		Player player = getPlayerController().getPlayerById(getPlayerId(user));
		if(player == null)
			return;

		player.buyJackpotNextRound(jackpotId);
	}
}
