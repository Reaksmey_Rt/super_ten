package com.sukagame.superten.roomclientrequestevent;



import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;

import com.sukagame.superten.database.model.PlayerInfo;
import com.sukagame.superten.gamelogic.Player;
//import com.sukagame.superten.gamelogic.SeatManager;
import com.sukagame.superten.send.PlayerJoin;
import com.sukagame.superten.send.Seat;
import com.sukagame.superten.util.GameUtil;

public class OnRequestBuyInHandler extends BaseSukaRoomClientRequestHandler {
	Player player;

	@Override
	public void handleClientRequest(User user, ISFSObject params) {
		super.handleClientRequest(user, params);

		PlayerInfo playerInfo = GameUtil.getPlayerInfo(user);
		player = getPlayerController().getPlayerById(playerInfo.getId());
		double buyInAmount = params.getDouble("value");

	

		if (buyInAmount > playerInfo.getBalance() ||  getTable().seatManager.isSeatAvailable(player.getServerSeat())) {
			new Seat(roomExtension).sendMessage(player, "seat not available !");
			return;
		}

		player.setBuyInAmount(buyInAmount);
		System.out.println("Get Buy In Amount : " + buyInAmount);

		player.setIsPlaying(!isGamePlaying());
		player.setTable(getTable());
		player.setServerSeat(player.getRequestSeat());
		getPlayerController().updatePlayerCount();
		new PlayerJoin(roomExtension).sendSelf(player);
		new PlayerJoin(roomExtension).sendAll();

		if (getPlayerController().getSeateds().size() == 1) {
			player.setIsDealer(true);
		}

	}

}