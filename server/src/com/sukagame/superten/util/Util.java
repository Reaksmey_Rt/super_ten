package com.sukagame.superten.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import com.sukagame.global.GameManager;
import com.sukagame.global.RoomCommand;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class Util
{
	public static String getFormatNumber(String prefix,int id)
	{
		String code = String.format("%06d", id);
		return prefix + code;
	}
	
	public static String getGamePlayId(String roomId,int gameCount)
	{
		return String.format(roomId + "-G%06d", gameCount);
	}
	
	public static String getGamePlayCode(int gameCount)
	{
		return String.format("G%06d", gameCount);
	}
	
	public static String getBalanceHistoryId(String playerId,String gameId)
	{
		String timeStamp = String.valueOf(new java.util.Date().getTime());
		return playerId +"-" + gameId + "-" + timeStamp;
	}
	
	public static void sendErrorCode(String reason, User target) {
		ISFSObject response = new SFSObject();
		response.putUtfString("message", reason);
		GameManager.getInstance().roomExtension.send(RoomCommand.CMD_ERROR, response, target);
	}

	public static void sendErrorCode(int code, User target) {
		ISFSObject response = new SFSObject();
		response.putInt("code", code);
		GameManager.getInstance().zone.getExtension().send(RoomCommand.CMD_ERROR, response, target);
	}
	
	public static void sendErrorCode(int code, User target, String... messageParams) {
		ISFSObject response = new SFSObject();
		response.putInt("code", code);
		response.putUtfStringArray("params", Arrays.asList(messageParams));
		GameManager.getInstance().zone.getExtension().send(RoomCommand.CMD_ERROR, response, target);
	}
	
	public static void broadCast(String username, String message) {
		ISFSObject response = new SFSObject();
		response.putUtfString("message", message);
		GameManager.getInstance().zone.getExtension().send(RoomCommand.CMD_BROADCAST_MESSAGE, response, (List<User>) GameManager.getInstance().roomExtension.getParentZone().getUserList());
	}
	
	public static String getUniqueString(int n) 
	{ 
		String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "0123456789" + "abcdefghijklmnopqrstuvxyz";

		StringBuilder sb = new StringBuilder(n); 
		for (int i = 0; i < n; i++) { 
			int index = (int)(AlphaNumericString.length() * Math.random()); 
			sb.append(AlphaNumericString .charAt(index)); 
		} 
		return sb.toString(); 
	} 
	
 	public static String getLocalTime()
	{
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+07:00"));
		cal.add(Calendar.HOUR, 7);
		java.util.Date currentLocalTime = cal.getTime();
		DateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		date.setTimeZone(TimeZone.getTimeZone("GMT+07:00")); 
		String localTime = date.format(currentLocalTime); 
		return localTime;
	}
}
