package com.sukagame.superten.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

import com.sukagame.global.GameManager;
import com.sukagame.global.Key;
import com.sukagame.superten.database.SukaDatabaseManager;
import com.sukagame.superten.database.model.GameEnable;
import com.sukagame.superten.database.model.JackpotLimit;
import com.sukagame.superten.database.model.Maintenance;
import com.sukagame.superten.database.model.PlayerInfo;
import com.sukagame.superten.database.model.Room;
import com.sukagame.superten.gamelogic.Card;
import com.sukagame.superten.gamelogic.ComputerPlayer;
import com.sukagame.superten.gamelogic.GameTemplate;
import com.sukagame.superten.gamelogic.HumanPlayer;
import com.sukagame.superten.gamelogic.Player;
import com.sukagame.superten.gamelogic.PlayerController;
import com.sukagame.superten.gamelogic.GameTemplate.STATE;
import com.sukagame.superten.room.RoomExtension;
import com.sukagame.superten.send.Waiting;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.variables.RoomVariable;
import com.smartfoxserver.v2.entities.variables.SFSRoomVariable;

public class GameUtil
{
	public static Player createPlayer(PlayerInfo playerInfo,User user,PlayerController playerController,Room room)
	{
		Player player = playerInfo.isRobot() ? new ComputerPlayer(playerController,room) : new HumanPlayer(playerController,room);
		player.setUser(user);
		player.setPlayerInfo(playerInfo);
		player.setIsPlaying(false);
		return player;
	}
	
	public static List<RoomVariable> AddRoomVariable(Room room)
	{
		ArrayList<RoomVariable> rv = new ArrayList<RoomVariable>();
		rv.add(new SFSRoomVariable("id", room.id,false,true,true));
		rv.add(new SFSRoomVariable("room_type_id",room.roomTypeId,false,true,true));
		rv.add(new SFSRoomVariable("room_type_name",room.roomTypeName,false,true,true));
		rv.add(new SFSRoomVariable("room_limit_type_id", room.roomLimitTypeId,false,true,true));
		rv.add(new SFSRoomVariable("room_limit_type_name", room.roomLimitTypeName,false,true,true));
		rv.add(new SFSRoomVariable("name", room.name,false,true,true));
		rv.add(new SFSRoomVariable("password", room.password));
		rv.add(new SFSRoomVariable("stake", room.stake,false,true,true));
		rv.add(new SFSRoomVariable("min_balance", room.minBalance,false,true,true));
		rv.add(new SFSRoomVariable("min_buy_in", room.buyInMinAmount,false,true,true));
		rv.add(new SFSRoomVariable("max_buy_in", room.buyInMaxAmount,false,true,true));
		rv.add(new SFSRoomVariable("bet_min_amt", room.betMinAmout,false,true,true));
		rv.add(new SFSRoomVariable("player_count", 0,false,true,true));
		rv.add(new SFSRoomVariable("is_custom_room",room.isCustomRoom,false,true,true));
		rv.add(new SFSRoomVariable("round",room.round,false,true,true));
		
		for(int i = 1; i <= 3; i++)
		{
			JackpotLimit jackpotLimit = room.jackpotLimits.get(i -1);
			rv.add(new SFSRoomVariable(Integer.toString(i),jackpotLimit.amount,false,true,true));
		}
		
		return rv;
	}
	
	public static Room getRoom(com.smartfoxserver.v2.entities.Room room)
	{
		String id = room.getVariable("id").getStringValue();
		int roomTypeId = room.getVariable("room_type_id").getIntValue();
		String roomTypeName = room.getVariable("room_type_name").getStringValue();
		int roomLimitTypeId = room.getVariable("room_limit_type_id").getIntValue();
		String roomLimitTypeName = room.getVariable("room_limit_type_name").getStringValue();
		String name = room.getVariable("name").getStringValue();
		String password = room.getVariable("password").getStringValue();
		double stake = room.getVariable("stake").getDoubleValue();
		double minBalance = room.getVariable("min_balance").getDoubleValue();
		double minBuyIn = room.getVariable("min_buy_in").getDoubleValue();
		double maxBuyIn = room.getVariable("max_buy_in").getDoubleValue();
		double betMinAmt = room.getVariable("bet_min_amt").getDoubleValue();
		boolean isCustomRoom = room.getVariable("is_custom_room").getBoolValue();
		String round = room.getVariable("round").getStringValue();
		
		Room resultRoom = new Room(id, roomTypeId,roomTypeName, roomLimitTypeId,roomLimitTypeName, name, "", password, 0, stake, false, minBalance,minBuyIn,maxBuyIn,betMinAmt,isCustomRoom,round);
		List<JackpotLimit> jackpotLimits = new ArrayList<JackpotLimit>();
		for(int i = 1; i <= 3; i++)
		{
			double amount = room.getVariable(Integer.toString(i)).getDoubleValue();
			JackpotLimit jackpotLimit = new JackpotLimit(i,0,amount);
			jackpotLimits.add(jackpotLimit);
		}
		resultRoom.jackpotLimits = jackpotLimits;
		return resultRoom;
	}
	
	public static void leaveRoom(Player player,RoomExtension roomExtension)
	{
		if(player.getUser().getLastJoinedRoom() != null)
			roomExtension.getApi().leaveRoom(player.getUser(), player.getUser().getLastJoinedRoom());
	}
	
	public static void leaveRoom(User user,RoomExtension roomExtension)
	{
		roomExtension.getApi().leaveRoom(user,user.getLastJoinedRoom());
	}
	
	public static void removeRoom(RoomExtension roomExtension)
	{
		roomExtension.getApi().removeRoom(roomExtension.getGameRoom());
	}

	public static String getRoomId(RoomExtension roomExtension)
	{
		return roomExtension.getGameRoom().getVariable("id").getStringValue();
	}

	public static void sleep(Integer miliSecond)
	{
		try
		{
			Thread.sleep(miliSecond);
		} catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}
	
	public static void wait(int second,RoomExtension roomExtension)
	{
		Integer maxTime = second;
		while(second >= 0)
		{
			new Waiting(roomExtension).send(second,maxTime);
			sleep(1000);
			second --;
		}
	}
	
	public static void waitMili(Integer mili)
	{
		sleep(mili);
	}
	
	public static ISFSArray toSFSObject(List<Card> cards)
	{
		Collections.sort(cards);
		ISFSArray params = new SFSArray();
		for(int i = 0; i < cards.size();i++)
		{
			params.addSFSObject(cards.get(i).toSFSObject());
		}
		return params;
	}
	
	public static boolean isGamePlaying(GameTemplate gameTemplate)
	{
		if(gameTemplate.getState() != STATE.WAITING)
			return true;
		return false;
	}
	
	public static PlayerInfo getPlayerInfo(User user)
	{
		String pId = (String)user.getSession().getProperty(Key.KEY_PLAYER_ID);
		String code = (String)user.getSession().getProperty(Key.KEY_PLAYER_CODE);
		String companyId = (String)user.getSession().getProperty(Key.KEY_COMPANY_ID);
		String companyName = (String)user.getSession().getProperty(Key.KEY_COMPANY_NAME);
		String providerName = (String)user.getSession().getProperty(Key.KEY_PROVIDER_NAME);
		int avatarId = (int)user.getSession().getProperty(Key.KEY_AVATAR_ID);
		String referralId = (String)user.getSession().getProperty(Key.KEY_PLAYER_REFERRAL_ID);
		String username = (String)user.getSession().getProperty(Key.KEY_PLAYER_USERNAME);
		String name = (String)user.getSession().getProperty(Key.KEY_PLAYER_NAME);
		String salt = (String)user.getSession().getProperty(Key.KEY_PLAYER_SALT);
		String pinCode = (String)user.getSession().getProperty(Key.KEY_PLAYER_PIN_CODE);
		boolean isRobot = (boolean)user.getSession().getProperty(Key.KEY_SESSION_IS_ROBOT);
		double balance = (double)user.getSession().getProperty(Key.KEY_PLAYER_BALANCE);
		if(isRobot)
			balance = topUpRobotBalance(companyId, pId, balance, user);
		
		return new PlayerInfo(pId, code, companyId,companyName,providerName, avatarId, referralId, username,"", name, isRobot, salt,pinCode, balance);
	}
	
	public static boolean isGameActive(User user)
	{
		return isGameEnable(user) && !isGameMaintenance(user);
	}
	
	public static void stopPlay(User user)
	{
		GameManager.getInstance().zone.getExtension().handleInternalMessage("stop-play", user);
	}
	
	public static void setBalance(User user)
	{
		GameManager.getInstance().zone.getExtension().handleInternalMessage("set_balance", user);
	}
	
	public static double round(double value, int places) {
		if (places < 0)
			return value;
		if(Double.isNaN(value))
			return 0;
		try
		{
			BigDecimal bd = new BigDecimal(value);
			bd = bd.setScale(places, RoundingMode.HALF_UP);
			return bd.doubleValue();
		} catch (Exception e)
		{
			System.out.println("@@@@@@@@@@@@@@@@@@@ round value error : " + value);
		}
		return value;
	}
	
	private static double topUpRobotBalance(String companyId, String playerId,double balance,User user)
	{
		double robotMinBalance = 500000000;
		double robotAddBalance = 1000000000;
		if(balance < robotMinBalance)
		{
			double playerBalance = balance + robotAddBalance;
			setSessionBalance(user, playerBalance);
			return playerBalance;
		}
		return balance;
	}
	
	private static void setSessionBalance(User user,double balance)
	{
		user.getSession().setProperty(Key.KEY_PLAYER_BALANCE, balance);
	}
	
	private static boolean isGameMaintenance(User user)
	{
		SukaDatabaseManager sukaDatabaseManager = new SukaDatabaseManager();
		PlayerInfo playerInfo = getPlayerInfo(user);
		List<Maintenance> maintenances = sukaDatabaseManager.getGameMaintenances();
		for (Maintenance maintenance : maintenances)
		{
			if(maintenance.companyId.equals(playerInfo.getCompanyId()) && maintenance.gameId.equals("G02"))
				return true;
		}
		return false;
	}
	
	private static boolean isGameEnable(User user)
	{
		SukaDatabaseManager sukaDatabaseManager = new SukaDatabaseManager();
		PlayerInfo playerInfo = getPlayerInfo(user);
		List<GameEnable> gameEnables = sukaDatabaseManager.getGameNameEnable(playerInfo.getCompanyId());
		for (GameEnable gameEnable : gameEnables)
		{
			if(gameEnable.gameId.equals("G02"))
				return true;
		}
		return false;
	}
	
	public static String format(double value)
	{
		return String.format("%,.2f", value);
	}
	
	public static <T> List<T> removeDuplicates(List<T> list)
	{
		List<T> newList = new ArrayList<T>();
		for (T element : list) {
			if (!newList.contains(element)) {
				newList.add(element);
			}
		}
		return newList;
	}
}
