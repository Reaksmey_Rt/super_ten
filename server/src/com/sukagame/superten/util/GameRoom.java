package com.sukagame.superten.util;

import java.util.EnumSet;

import com.sukagame.global.GameManager;
import com.sukagame.global.Key;
import com.sukagame.superten.database.GameDatabaseManager;
import com.sukagame.superten.error.CustomErrorCode;
import com.smartfoxserver.v2.api.CreateRoomSettings;
import com.smartfoxserver.v2.api.ISFSApi;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.SFSRoomRemoveMode;
import com.smartfoxserver.v2.entities.SFSRoomSettings;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.Zone;
import com.smartfoxserver.v2.exceptions.SFSCreateRoomException;
import com.smartfoxserver.v2.exceptions.SFSJoinRoomException;

public class GameRoom
{
	private static final String extensionClass = "com.sukagame.superten.room.RoomExtension";
	public static final String superten = "superten";
	public static int maxPlayer = 8;
	public static int maxSpectator = 15;
	
	public static Room create(com.sukagame.superten.database.model.Room room, ISFSApi api, Zone zone, User user)
	{
		int roomNameMinLength = zone.getExtension().getParentZone().getMinRoomNameChars();
		int roomNameMaxLength = zone.getExtension().getParentZone().getMaxRoomNameChars();
		int clientRoomNameLength = room.name.toCharArray().length;
		
		if(clientRoomNameLength < roomNameMinLength || clientRoomNameLength > roomNameMaxLength)
		{
			Util.sendErrorCode(CustomErrorCode.ROOM_BAD_NAME.getId(), user,Integer.toString(roomNameMinLength),Integer.toString(roomNameMaxLength));
			return null;
		}
		
		CreateRoomSettings cfg = new CreateRoomSettings();

		cfg.setName(room.name);
		cfg.setMaxUsers(maxPlayer);
		cfg.setMaxVariablesAllowed(30);
		cfg.setMaxSpectators(maxSpectator);
		cfg.setGroupId(superten);
		
		cfg.setGame(true);
		cfg.setDynamic(true);
		cfg.setRoomVariables(GameUtil.AddRoomVariable(room));
		cfg.setAutoRemoveMode(SFSRoomRemoveMode.NEVER_REMOVE);
		if(room.password != null && !room.password.equals(""))
			cfg.setPassword(room.password);
		
		cfg.setRoomSettings(EnumSet.of(SFSRoomSettings.USER_ENTER_EVENT, SFSRoomSettings.USER_EXIT_EVENT, SFSRoomSettings.USER_COUNT_CHANGE_EVENT, SFSRoomSettings.USER_VARIABLES_UPDATE_EVENT));
		cfg.setExtension(new CreateRoomSettings.RoomExtensionSettings(superten,extensionClass));

		Room createdRoom = null;
		try 
		{
			if(user != null)
				createdRoom = api.createRoom(zone, cfg, user,true,user.getLastJoinedRoom());
			else
				createdRoom = api.createRoom(zone, cfg, user);
			new GameDatabaseManager().AddRoom(room);
		} 
		catch (SFSCreateRoomException e) 
		{
			System.out.println("error : " + e.getMessage());
			Util.sendErrorCode(CustomErrorCode.ROOM_CREATE_ERROR.getId(), user);
		}
		
		return createdRoom;
	}

	public static Room join(User user, Room room, String password)
	{
		GameManager gameManager = GameManager.getInstance();
		if(room == null)
		{
			Util.sendErrorCode(CustomErrorCode.ROOM_DOES_NOT_EXIST.getId(), user);
			return room;
		}
		
		if(!isEnoughtBalance(room, user))
		{
			Util.sendErrorCode(CustomErrorCode.NOT_ENOUGH_MONEY_TO_JOIN_ROOM.getId(), user);
			return room;
		}
		
		int playerCount = room.getVariable(Key.ROOM_PLAYER_COUNT).getIntValue();
		int roomPlayerCount = room.getPlayersList().size();
		if(playerCount > gameManager.maxPlayer || roomPlayerCount >= gameManager.maxPlayer)
		{
			Util.sendErrorCode(CustomErrorCode.ROOM_FULL.getId(), user,room.getName());
			return room;
		}
		
		if(room.getPassword()!= null && !room.getPassword().equals(password))
		{
			Util.sendErrorCode(CustomErrorCode.ROOM_INCORRECT_PASSWORD.getId(), user,room.getName());
			return room;
		}
		
		try
		{
			gameManager.api.joinRoom(user, room,password,true,user.getLastJoinedRoom(),true,true);
		} catch (SFSJoinRoomException e)
		{
			System.out.println("join room error " + e.getMessage());
			Util.sendErrorCode(CustomErrorCode.ROOM_JOIN_ERROR.getId(), user);
		}
		
		return room;
	}
	
	private static boolean isEnoughtBalance(Room room,User user)
	{
		com.sukagame.superten.database.model.Room roomInfo = GameUtil.getRoom(room);
		double balance = GameUtil.getPlayerInfo(user).getBalance();
		return balance >= roomInfo.minBalance ? true : false;
	}
}
