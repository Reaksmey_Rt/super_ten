package com.sukagame.superten.room;

import com.sukagame.global.GameManager;
import com.sukagame.superten.gamelogic.GameTemplate;
import com.sukagame.superten.gamelogic.PlayerController;
import com.smartfoxserver.v2.entities.SFSRoomRemoveMode;
import com.smartfoxserver.v2.entities.User;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class TaskRunner implements Runnable {

	private AtomicReference<RoomExtension> roomExtensionAtomic;
	@SuppressWarnings("unused")
	private AtomicReference<PlayerController> playerControllerAtomic;
	private AtomicReference<List<User>> usersAtomic;

	public TaskRunner(AtomicReference<RoomExtension> roomExtensionAtomic,
			AtomicReference<PlayerController> playerControllerAtomic, AtomicReference<List<User>> usersAtomic) {
		this.roomExtensionAtomic = roomExtensionAtomic;
		this.playerControllerAtomic = playerControllerAtomic;
		this.usersAtomic = usersAtomic;
	}

	@Override
	public void run() {
		try {
			if (isCanStartGame()) {
				roomExtensionAtomic.get().getGameTemplate().Play();

			} 
			else if (isNoPlayer() && !isStaticRoom()) {
				roomExtensionAtomic.get().getGameTemplate().Remove();
			} 
			else if (roomExtensionAtomic.get().getGameTemplate().getState() == GameTemplate.STATE.WAITING) 
			{
				roomExtensionAtomic.get().getGameTemplate().Clear();
			}

			List<User> users = usersAtomic.get();
			for (int i = 0; i < users.size(); i++) {
				User user = users.get(i);
				GameManager.getInstance().zone.getExtension().handleInternalMessage("stop-play", user);
				users.remove(user);
			}
		} catch (Exception ex) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			ex.printStackTrace(pw);
			System.out.println("task runner error : " + sw.toString());
		}
		// System.out.println("STATE GAME : " +
		// roomExtensionAtomic.get().getGameTemplate().getStateName());
	}

	private boolean isStaticRoom() {
		return roomExtensionAtomic.get().getGameRoom().getAutoRemoveMode() == SFSRoomRemoveMode.NEVER_REMOVE;
	}

	private boolean isNoPlayer() {
		RoomExtension roomExtension = roomExtensionAtomic.get();
		return roomExtension.getTable().roomExtension.getGameRoom().getUserList().size() == 0
				&& roomExtension.getGameTemplate().getState() == GameTemplate.STATE.WAITING;
	}

	private boolean isCanStartGame() {
		RoomExtension roomExtension = roomExtensionAtomic.get();
		if (roomExtension.getGameTemplate().getState() == GameTemplate.STATE.WAITING
				&& roomExtension.getPlayerController().isEnoughPlayer( ) )
			return true;
		return false;
	}
}
