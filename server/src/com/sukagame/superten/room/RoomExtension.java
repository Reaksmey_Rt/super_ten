package com.sukagame.superten.room;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import com.sukagame.global.GameManager;
import com.sukagame.global.Key;
import com.sukagame.global.RoomCommand;
import com.sukagame.superten.database.model.PlayerInfo;
import com.sukagame.superten.gamelogic.*;
import com.sukagame.superten.roomclientrequestevent.*;
import com.sukagame.superten.roomserverevent.*;

import com.sukagame.superten.util.GameUtil;
import com.smartfoxserver.v2.core.SFSEventType;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.variables.RoomVariable;
import com.smartfoxserver.v2.entities.variables.SFSRoomVariable;
import com.smartfoxserver.v2.extensions.SFSExtension;

public class RoomExtension extends SFSExtension
{
	private GameTemplate gameTemplate;
	private Game game = null;
	private PlayerController playerController;
	public AtomicReference<List<User>> usersAtomic;
	@SuppressWarnings("unused")
	private ScheduledFuture<?> taskHandle ;

	@Override
	public void destroy()
	{
		super.destroy();
	}

	@Override
	public Object handleInternalMessage(String cmdName, Object params)
	{
		ISFSObject data = new SFSObject();
		if (cmdName.equals("update_maintenance")) 
		{
			getGame().isGameInactive = true;
		}
		else if (cmdName.equals("update_game_enable"))
		{
			getGame().isGameInactive = true;
		}
		else if(cmdName.equals("rejoin"))
		{
			User request=(User)params;
			PlayerInfo requestInfo = GameUtil.getPlayerInfo(request);
			if(getGameRoom().containsProperty(Key.KEY_USERS))
			{
				@SuppressWarnings("unchecked")
				List<User> users = (ArrayList<User>)getGameRoom().getProperty(Key.KEY_USERS);
				for (User user : users)
				{
					PlayerInfo playerInfo = GameUtil.getPlayerInfo(user);
					if(playerInfo.getId().equals(requestInfo.getId()))
						return user;
				}
			}
			return null;
		}
		return data;
	}
	
	@Override
	public void init()
	{
		GameManager.getInstance().roomExtension = this;
		//server event handler
		addEventHandler(SFSEventType.USER_LEAVE_ROOM, OnUserLeaveRoomHandler.class);
		addEventHandler(SFSEventType.USER_DISCONNECT, OnUserDisconnectHandler.class);
		addEventHandler(SFSEventType.USER_JOIN_ROOM, OnUserJoinRoomHandler.class);
		
		addRequestHandler(RoomCommand.CMD_REQUEST_VALIDATE_HAND, OnRequestValidateCards.class);
		addRequestHandler(RoomCommand.CMD_REQUEST_CONFIRM_CARDS_RANKED, OnRequestConfirmCardRankedHandler.class);
		addRequestHandler(RoomCommand.CMD_REQUEST_LEAVE_ROOM, OnRequestLeaveRoomHandler.class);
		addRequestHandler(RoomCommand.CMD_REQUEST_EMOJI, OnRequestSendEmojiHandler.class);
		addRequestHandler(RoomCommand.CMD_BUY_GAME_JACKPOT, OnRequestBuyJackpotHandler.class);
		addRequestHandler(RoomCommand.CMD_REQUEST_SEAT, OnRequestSitHandler.class);
		addRequestHandler(RoomCommand.CMD_BUY_IN, OnRequestBuyInHandler.class);
		
		addRequestHandler(RoomCommand.CMD_REQUEST_STAND, OnRequestStandHandler.class);

		usersAtomic = new AtomicReference<List<User>>();
		List<User> users = new ArrayList<User>();
		usersAtomic.set(users);

		AtomicReference<RoomExtension> roomExtensionAtomic = new AtomicReference<RoomExtension>();
		roomExtensionAtomic.set(this);
		playerController = new PlayerController(roomExtensionAtomic);
		AtomicReference<PlayerController> playerControllerAtomic = new AtomicReference<PlayerController>();
		playerControllerAtomic.set(playerController);

		GameTemplate gameTemplate = new GameTemplate(this);
		setGameTemplate(gameTemplate);

		ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
		taskHandle = executor.scheduleAtFixedRate(new TaskRunner(roomExtensionAtomic,playerControllerAtomic,usersAtomic), 0, 100, TimeUnit.MILLISECONDS);
	}
	
	public void updateRoomVariableStatus(String value)
	{
		ArrayList<RoomVariable> rv = new ArrayList<RoomVariable>();
		rv.add(new SFSRoomVariable("state", value));
		getApi().setRoomVariables(null, getGameRoom(), rv);
	}
	
	public void updateRoomVariable(String gamePlayId,String roundId)
	{
		ArrayList<RoomVariable> rv = new ArrayList<RoomVariable>();
		rv.add(new SFSRoomVariable("game_play_id", gamePlayId));
		rv.add(new SFSRoomVariable("round", roundId));
		getApi().setRoomVariables(null, getGameRoom(), rv);
	}

	public Room getGameRoom()
	{
		return getParentRoom();
	}
	
	public void setGameTemplate(GameTemplate gameTemplate)
	{
		this.gameTemplate = gameTemplate;
	}
	
	public GameTemplate getGameTemplate()
	{
		return this.gameTemplate;
	}
	
	public void setGame(Game game)
	{
		this.game = game;
	}
	
	public Game getGame()
	{
		return game;
	}

	public Table getTable()
	{
		return this.gameTemplate.getGame().getTable();
	}

	public PlayerController getPlayerController()
	{
		return this.playerController;
	}
	
	public void send(String cmd, String json)
	{
		SFSObject param = new SFSObject();
		param.putUtfString(Key.KEY_PARAM, json);
		send(cmd, param,getGameRoom().getUserList());
	}
	
	public void send(String cmd, String json, User user)
	{
		SFSObject param = new SFSObject();
		param.putUtfString(Key.KEY_PARAM, json);
		send(cmd, param, user);
	}
	
	public void sendSpec(String cmd, String json)
	{
		SFSObject param = new SFSObject();
		param.putUtfString(Key.KEY_PARAM, json);
		try
		{
			send(cmd, param,getGameRoom().getSpectatorsList());
		} catch (NullPointerException e)
		{
			e.getStackTrace();
		}
	}
	
	public void sendPlayer(String cmd, String json)
	{
		SFSObject param = new SFSObject();
		param.putUtfString(Key.KEY_PARAM, json);
		try
		{
			send(cmd, param,getGameRoom().getPlayersList());
		} catch (NullPointerException e)
		{
			e.getStackTrace();
		}
	}
}
