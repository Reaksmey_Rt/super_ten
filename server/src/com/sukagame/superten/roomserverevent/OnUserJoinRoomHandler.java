package com.sukagame.superten.roomserverevent;

import com.smartfoxserver.v2.core.ISFSEvent;
import com.smartfoxserver.v2.core.SFSEventParam;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.exceptions.SFSException;

public class OnUserJoinRoomHandler extends BaseSukaRoomServerEventHandler
{
	@Override
	public void handleServerEvent(ISFSEvent event) throws SFSException
	{
		super.handleServerEvent(event);
		User user = (User) event.getParameter(SFSEventParam.USER);
		roomExtension.getPlayerController().join(user);
	}
}
