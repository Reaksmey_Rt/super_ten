package com.sukagame.superten.roomserverevent;

import com.smartfoxserver.v2.core.ISFSEvent;
import com.smartfoxserver.v2.core.SFSEventParam;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.exceptions.SFSException;
import com.smartfoxserver.v2.extensions.BaseServerEventHandler;
import com.sukagame.superten.room.RoomExtension;

public class BaseSukaRoomServerEventHandler extends BaseServerEventHandler
{
	public RoomExtension roomExtension = null;
	public User user = null;
	
	@Override
	public void handleServerEvent(ISFSEvent event) throws SFSException
	{
		roomExtension = (RoomExtension)getParentExtension();
		user = (User) event.getParameter(SFSEventParam.USER);
	}
}
