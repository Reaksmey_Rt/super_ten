package com.sukagame.superten.roomserverevent;

import com.smartfoxserver.v2.core.ISFSEvent;
import com.smartfoxserver.v2.core.SFSEventParam;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.exceptions.SFSException;
import com.sukagame.superten.database.model.Room;
import com.sukagame.superten.gamelogic.GameTemplate;
import com.sukagame.superten.gamelogic.Player;
import com.sukagame.superten.gamelogic.GameTemplate.STATE;
import com.sukagame.superten.util.GameUtil;

public class OnUserDisconnectHandler extends BaseSukaRoomServerEventHandler
{
	@Override
	public void handleServerEvent(ISFSEvent event) throws SFSException
	{
		super.handleServerEvent(event);
		User user = (User) event.getParameter(SFSEventParam.USER);
		String pId = GameUtil.getPlayerInfo(user).getId();
		Player player = roomExtension.getPlayerController().getPlayerById(pId);
		if(player != null)
		{
			if(isCanRemove(player))
			{
				roomExtension.getPlayerController().Remove(player);
				GameUtil.stopPlay(user);
			}
			else 
				roomExtension.getPlayerController().addReconnectUser(player);
		}
		else 
		{
			GameUtil.stopPlay(user);
			return;
		}
		
		if(isRemoveRoom())
			GameUtil.removeRoom(roomExtension);
	}
	
	private boolean isCanRemove(Player player)
	{
		if(gameBig2().getState() == STATE.WAITING || player.getIsPlaying() == false || player.getServerSeat() == -1)
			return true;
		return false;
	}

	private boolean isRemoveRoom()
	{
		Room roomData = GameUtil.getRoom(roomExtension.getGameRoom());
		int sfsUserCount = roomExtension.getGameRoom().getUserList().size();
		int customUserCount = roomExtension.getPlayerController().getPlayers().size();
		return sfsUserCount == 0 && customUserCount == 0 && roomData.isCustomRoom;
	}
	
	private GameTemplate gameBig2()
	{
		return roomExtension.getGameTemplate();
	}
}
