package com.sukagame.superten.database;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import com.sukagame.superten.database.model.PlayBetResult;
import com.sukagame.superten.zone.event.OnServerReadyHandler;

public class ProviderDatabaseManager
{
	private final String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
	
	public void AddBetReport(PlayBetResult playBetResult, List<String> providers)
	{
		for (String provider : providers)
		{
			if(playBetResult.providerName.equals(provider))
				AddBetReport(playBetResult,provider);
		}
	}
	
	public void AddBetReport(PlayBetResult playBetResult,String provider)
	{
		Connection connection = getConnection(provider);
		try
		{
			PreparedStatement smt = connection.prepareStatement("{call susun.AddBetReport(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			smt.setString(1,playBetResult.gamePlayId);
			smt.setString(2,playBetResult.companyId);
			smt.setString(3,playBetResult.companyName);
			smt.setString(4,playBetResult.playerId);
			smt.setString(5,playBetResult.username);
			smt.setBoolean(6, playBetResult.isRobot);
			smt.setString(7,playBetResult.roomId);
			smt.setString(8,playBetResult.roomName);
			smt.setString(9,playBetResult.round);
			smt.setString(10,playBetResult.card);
			smt.setInt(11, playBetResult.isWinSpecial);
			smt.setString(12,playBetResult.points);
			smt.setString(13,playBetResult.handAmounts);
			smt.setInt(14, playBetResult.totalPoint);
			smt.setDouble(15, playBetResult.stake);
			smt.setDouble(16, playBetResult.totalBet);
			smt.setString(17, playBetResult.hand);
			smt.setString(18, playBetResult.handStatus);
			smt.setDouble(19, playBetResult.turnover);
			smt.setDouble(20, playBetResult.prize);
			smt.setString(21, playBetResult.status);
			smt.setDouble(22, playBetResult.amount);
			smt.setDouble(23, playBetResult.fee);
			smt.setDouble(24, playBetResult.shareCapital);
			smt.setDouble(25, playBetResult.commission);
			smt.setDouble(26, playBetResult.jackpotBuyAmount);
			smt.setDouble(27, playBetResult.jackpotWinAmount);
			smt.setDouble(28, playBetResult.lastBalance);
			smt.executeUpdate();
			connection.close();
		} catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				connection.close();
			} catch (SQLException e1)
			{
				e1.printStackTrace();
			}
		}
	}
	
	public Connection getConnection(String databaseName)
	{
		try
		{
			DriverManager.registerDriver((Driver) Class.forName(driver).newInstance());
			return DriverManager.getConnection(OnServerReadyHandler.getProviderConnection(databaseName));
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
}
