package com.sukagame.superten.database;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sukagame.superten.database.model.GameEnable;
import com.sukagame.superten.database.model.Maintenance;
import com.sukagame.superten.database.model.RoomDetailData;
import com.sukagame.superten.zone.event.OnServerReadyHandler;

public class SukaDatabaseManager
{
	private final String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
	
	public SukaDatabaseManager() {}
	
	public double getShareByCompanyAndGame(String companyId)
	{
		Connection connection = getConnection();
		double share = 0;
		try
		{
			PreparedStatement smt = connection.prepareStatement("{call dbo.GetShareByCompanyAndGame(?,?)}");
			smt.setString(1,companyId);
			smt.setString(2,"G03");
			ResultSet rs = smt.executeQuery();
			while (rs.next()) {
				share = rs.getDouble("pt");
			}
			
			connection.close();
		} catch (Exception e)
		{
			e.printStackTrace();
			addDatabaseErrorLog("getShareByCompanyAndGame", e.getMessage());
			try
			{
				connection.close();
			} catch (SQLException e1)
			{
				e1.printStackTrace();
			}
		}
		return share;
	}
	
	public double getFee()
	{
		Connection connection = getConnection();
		double fee = 0;
		try
		{
			PreparedStatement smt = connection.prepareStatement("{call dbo.GetFee(?)}");
			smt.setString(1,"G03");
			ResultSet rs = smt.executeQuery();
			while (rs.next()) {
				fee = rs.getDouble("fee");
			}
			
			connection.close();
		} catch (Exception e)
		{
			e.printStackTrace();
			addDatabaseErrorLog("getFee", e.getMessage());
			try
			{
				connection.close();
			} catch (SQLException e1)
			{
				e1.printStackTrace();
			}
		}
		return fee;
	}
	
	public List<Maintenance> getGameMaintenances()
	{
		List<Maintenance> maintenances = new ArrayList<Maintenance>();
		Connection connection = getConnection();
		try
		{
			PreparedStatement smt = connection.prepareStatement("{call dbo.GetGameMaintenance()}");
			ResultSet rs = smt.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				String gameId = rs.getString("game_id");
				String companyId = rs.getString("company_id");
				String gameName = rs.getString("game_name");
				String startDate = rs.getString("start_date");
				String endDate = rs.getString("end_date");
				String description = rs.getString("description");
  				maintenances.add(new Maintenance(id,gameId,companyId,gameName, startDate,endDate,description));
			}
			connection.close();
		} catch (Exception e)
		{
			e.printStackTrace();
			addDatabaseErrorLog("getGameMaintenances", e.getMessage());
			try
			{
				connection.close();
			} catch (SQLException e1)
			{
				e1.printStackTrace();
			}
		}
		return maintenances;
	}
	
	public List<GameEnable> getGameNameEnable(String companyId)
	{
		List<GameEnable> gameEnables = new ArrayList<GameEnable>();
		Connection connection = getConnection();
		try
		{
			PreparedStatement smt = connection.prepareStatement("{call dbo.GetGameNameEnable(?)}");
			smt.setString(1,companyId);
			ResultSet rs = smt.executeQuery();
			while (rs.next()) {
				String id = rs.getString("id");
				String companyIdDB = rs.getString("company_id");
				String gameId = rs.getString("game_id");
				String gameName = rs.getString("game_name");
				gameEnables.add(new GameEnable(id,companyIdDB,gameId,gameName));
			}
			connection.close();
		} catch (Exception e)
		{
			e.printStackTrace();
			addDatabaseErrorLog("getGameNameEnable", e.getMessage());
			try
			{
				connection.close();
			} catch (SQLException e1)
			{
				e1.printStackTrace();
			}
		}
		return gameEnables;
	}

	public void updateProgressiveJackpot(double amount)
	{
		Connection connection = getConnection();
		try
		{
			PreparedStatement smt = connection.prepareStatement("{call dbo.UpdateProgressiveJackpot(?)}");
			smt.setDouble(1,amount);
			smt.executeUpdate();
			connection.close();
		} catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				connection.close();
			} catch (SQLException e1)
			{
				e1.printStackTrace();
			}
		}
	}
	
	public RoomDetailData getRoomDetailData(int roomDetailId)
	{
		Connection connection = getConnection();
		try
		{
			PreparedStatement smt = connection.prepareStatement("{call susun.GetRoomGroupLimitDetailById(?)}");
			smt.setInt(1,roomDetailId);
			ResultSet rs = smt.executeQuery();
			while (rs.next()) {
				int roomTypeId = rs.getInt("room_type_id");
				String name = rs.getString("name");
				int roomLimitTypeId = rs.getInt("room_limit_type_id");
				String limitName = rs.getString("limit_name");
				double minBalance = rs.getDouble("min_balance");
				double buyInMinAmt = 2000;
				double buyInMaxAmt = 70000;
				double betInAmt = 200;
				double stake = rs.getDouble("stake");
				return new RoomDetailData(roomTypeId,name,roomLimitTypeId,limitName,minBalance,buyInMinAmt,buyInMaxAmt,betInAmt,stake);
			}
			connection.close();
		} catch (Exception e)
		{
			e.printStackTrace();
			addDatabaseErrorLog("getRoomDetailData", e.getMessage());
			try
			{
				connection.close();
			} catch (SQLException e1)
			{
				e1.printStackTrace();
			}
		}
		return new RoomDetailData();
	}
	
	private void addDatabaseErrorLog(String funtionName,String errorMessage)
	{
		Connection connection = getConnection();
		try
		{
			PreparedStatement smt = connection.prepareStatement("{call dbo.AddDatabaseErrorLog(?,?)}");
			smt.setString(1,funtionName);
			smt.setString(2,errorMessage);
			smt.executeUpdate();
			connection.close();
		} catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				connection.close();
			} catch (SQLException e1)
			{
				e1.printStackTrace();
			}
		}
	}
	
	private Connection getConnection()
	{
		try
		{
			DriverManager.registerDriver((Driver) Class.forName(driver).newInstance());
			return DriverManager.getConnection(OnServerReadyHandler.getSukagameConnection());
		} catch (Exception e)
		{
			e.printStackTrace();
			addDatabaseErrorLog("GetConnection", e.getMessage());
		}
		return null;
	}
}
