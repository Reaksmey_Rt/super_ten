package com.sukagame.superten.database;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.sukagame.superten.database.model.*;
import com.sukagame.superten.zone.event.OnServerReadyHandler;

public class GameDatabaseManager
{
	private final String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
	
	public enum STORE_PROCEDURE
	{
		GET_ROOM_LATEST_ID("{call dbo.GetRoomLatestId()}"),
		GET_GAMEPLAY_LATEST_ID("{call dbo.GetGamePlayLatestId()}");
		
		String store;
		STORE_PROCEDURE(String store)
		{
			this.store = store;
		}
		
		public String getStoreProcedure()
		{
			return this.store;
		}
	}
	
	public Integer getLatestId(STORE_PROCEDURE storeId)
	{
		Connection connection = getConnection();
		int id = 0;
		try
		{
			PreparedStatement smt = connection.prepareStatement(storeId.getStoreProcedure());
			ResultSet rs = smt.executeQuery();
			while (rs.next()) {
				id = rs.getInt("no");
			}
			connection.close();
		} catch (Exception e)
		{
			e.printStackTrace();
			addDatabaseErrorLog("getLatestId", e.getMessage());
			try
			{
				connection.close();
			} catch (SQLException e1)
			{
				e1.printStackTrace();
			}
		}
		return id + 1;
	}
	
	public List<Room> getRooms()
	{
		Connection connection = getConnection();
		List<Room> rooms = new ArrayList<Room>();
		try
		{
			PreparedStatement smt = connection.prepareStatement("{call dbo.GetRoomLists()}");
			ResultSet rs = smt.executeQuery();
			while (rs.next()) {
				int roomTypeId = rs.getInt("room_type_id");
				String roomTypeName = rs.getString("room_type_name");
				int roomLimitTypeId = rs.getInt("room_limit_type_id");
				String roomLimitTypeName = rs.getString("room_limit_type_name");
				String name = rs.getString("name");
				String password = rs.getString("password");
				int status = rs.getInt("status");
				double stake = rs.getDouble("stake");
				double minBalance = rs.getDouble("min_balance");
				double minBuyIn = 3000; //rs.getDouble("min_balance");
				double maxBuyIn = 50000;//rs.getDouble("min_balance");
				double betMinAmt = 200;
				
				Room room = new Room("",roomTypeId,roomTypeName, roomLimitTypeId,roomLimitTypeName, name,"",password
						,status,stake, true,minBalance,minBuyIn,maxBuyIn,betMinAmt,false,"G000001");
				rooms.add(room);
			}
			connection.close();
		} catch (Exception e)
		{
			e.printStackTrace();
			addDatabaseErrorLog("getRooms", e.getMessage());
			try
			{
				connection.close();
			} catch (SQLException e1)
			{
				e1.printStackTrace();
			}
		}
		return rooms;
	}
	
	public List<JackpotLimit> getJackpotLimits(int id)
	{
		List<JackpotLimit> jackpotLimits = new ArrayList<JackpotLimit>();
		Connection connection = getConnection();
		try
		{
			PreparedStatement smt = connection.prepareStatement("{call dbo.GetJackpotLimits(?)}");
			smt.setInt(1,id);
			ResultSet rs = smt.executeQuery();
			while (rs.next()) {
				int jackpotId = rs.getInt("id");
				int roomTypeId = rs.getInt("room_type_id");
				double amount = rs.getDouble("amount");
				JackpotLimit jackpotLimit = new JackpotLimit(jackpotId,roomTypeId,amount);
				jackpotLimits.add(jackpotLimit);
			}
			connection.close();
		} catch (Exception e)
		{
			e.printStackTrace();
			addDatabaseErrorLog("getJackpotLimits", e.getMessage());
			try
			{
				connection.close();
			} catch (SQLException e1)
			{
				e1.printStackTrace();
			}
		}
		return jackpotLimits;
	}
	
	public RoomLimitType getRoomLimitType(int roomLimitTypeId)
	{
		Connection connection = getConnection();
		RoomLimitType roomLimitType = null;
		try
		{
			PreparedStatement smt = connection.prepareStatement("{call dbo.GetRoomLimitTypes()}");
			ResultSet rs = smt.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				double minBalance = rs.getDouble("min_balance");
				double stake = rs.getDouble("stake");
				roomLimitType = new RoomLimitType(id,name,minBalance,stake);
			}
			connection.close();
		} catch (Exception e)
		{
			e.printStackTrace();
			addDatabaseErrorLog("getRoomsLimitType", e.getMessage());
			try
			{
				connection.close();
			} catch (SQLException e1)
			{
				e1.printStackTrace();
			}
		}
		return roomLimitType;
	}
	
	public List<RoomType> getRoomsType()
	{
		Connection connection = getConnection();
		List<RoomType> roomTypes = new ArrayList<RoomType>();
		try
		{
			PreparedStatement smt = connection.prepareStatement("{call dbo.GetRoomTypes()}");
			ResultSet rs = smt.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				RoomType roomType = new RoomType(id,name);
				roomTypes.add(roomType);
			}
			connection.close();
		} catch (Exception e)
		{
			e.printStackTrace();
			addDatabaseErrorLog("getRoomsType", e.getMessage());
			try
			{
				connection.close();
			} catch (SQLException e1)
			{
				e1.printStackTrace();
			}
		}
		return roomTypes;
	}
	
	public void AddRoom(Room room)
	{
		Connection connection = getConnection();
		try
		{
			PreparedStatement smt = connection.prepareStatement("{call dbo.AddRoom(?,?,?,?,?,?,?,?,?)}");
			smt.setString(1,room.id);
			smt.setString(2,room.name);
			smt.setString(3,room.ownerId);
			smt.setInt(4,room.roomTypeId);
			smt.setInt(5,room.roomLimitTypeId);
			smt.setString(6,room.password);
			smt.setInt(7,room.status);
			smt.setBoolean(8,room.isStatic);
			smt.setDouble(9, room.stake);
			smt.executeUpdate();
			connection.close();
		} catch (Exception e)
		{
			e.printStackTrace();
			addDatabaseErrorLog("AddRoom", e.getMessage());
			try
			{
				connection.close();
			} catch (SQLException e1)
			{
				e1.printStackTrace();
			}
		}
	}

	public void AddGamePlay(String id,String code,String roomId)
	{
		Connection connection = getConnection();
		try
		{
			PreparedStatement smt = connection.prepareStatement("{call dbo.AddGamePlay(?,?,?)}");
			smt.setString(1,id);
			smt.setString(2,code);
			smt.setString(3,roomId);
			smt.executeUpdate();
			connection.close();
		} catch (Exception e)
		{
			e.printStackTrace();
			addDatabaseErrorLog("AddGamePlay", e.getMessage());
			try
			{
				connection.close();
			} catch (SQLException e1)
			{
				e1.printStackTrace();
			}
		}
	}
	
	public void AddPlayerBetResult(PlayBetResult playBetResult)
	{
		Connection connection = getConnection();
		try
		{
			PreparedStatement smt = connection.prepareStatement("{call dbo.AddPlayerBetResult(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			smt.setString(1,playBetResult.gamePlayId);
			smt.setString(2,playBetResult.companyId);
			smt.setString(3,playBetResult.companyName);
			smt.setString(4,playBetResult.playerId);
			smt.setString(5,playBetResult.username);
			smt.setBoolean(6, playBetResult.isRobot);
			smt.setString(7,playBetResult.roomId);
			smt.setString(8,playBetResult.roomName);
			smt.setString(9,playBetResult.round);
			smt.setString(10,playBetResult.card);
			smt.setInt(11, playBetResult.isWinSpecial);
			smt.setString(12,playBetResult.points);
			smt.setString(13,playBetResult.handAmounts);
			smt.setInt(14, playBetResult.totalPoint);
			smt.setDouble(15, playBetResult.stake);
			smt.setDouble(16, playBetResult.totalBet);
			smt.setString(17, playBetResult.hand);
			smt.setString(18, playBetResult.handStatus);
			smt.setDouble(19, playBetResult.turnover);
			smt.setDouble(20, playBetResult.prize);
			smt.setString(21, playBetResult.status);
			smt.setDouble(22, playBetResult.amount);
			smt.setDouble(23, playBetResult.fee);
			smt.setDouble(24, playBetResult.shareCapital);
			smt.setDouble(25, playBetResult.commission);
			smt.setDouble(26, playBetResult.jackpotBuyAmount);
			smt.setDouble(27, playBetResult.jackpotWinAmount);
			smt.setDouble(28, playBetResult.lastBalance);
			smt.executeUpdate();
			connection.close();
		} catch (Exception e)
		{
			e.printStackTrace();
			addDatabaseErrorLog("AddPlayerBetResult", e.getMessage());
			try
			{
				connection.close();
			} catch (SQLException e1)
			{
				e1.printStackTrace();
			}
		}
	}
	
	public void addBalanceHistory(BalanceHistory balanceHistory)
	{
		Connection connection = getConnection();
		try
		{
			PreparedStatement smt = connection.prepareStatement("{call dbo.AddBalanceHistory(?,?,?,?,?,?,?)}");
			smt.setString(1,balanceHistory.id);
			smt.setString(2, balanceHistory.companyId);
			smt.setString(3,balanceHistory.playerId);
			smt.setString(4,balanceHistory.type);
			smt.setDouble(5,balanceHistory.amount);
			smt.setDouble(6,balanceHistory.lastBalance);
			smt.setString(7,balanceHistory.ip);
			smt.executeUpdate();
			connection.close();
		} catch (Exception e)
		{
			e.printStackTrace();
			addDatabaseErrorLog("addBalanceHistory", e.getMessage());
			try
			{
				connection.close();
			} catch (SQLException e1)
			{
				e1.printStackTrace();
			}
		}
	}
	
	public void addBalanceTransaction(BalanceTransaction tansaction)
	{
		Connection connection = getConnection();
		try
		{
			PreparedStatement smt = connection.prepareStatement("{call dbo.AddTransaction(?,?,?,?,?,?,?,?)}");
			smt.setString(1,tansaction.id);
			smt.setString(2, tansaction.companyId);
			smt.setString(3,tansaction.playerId);
			smt.setString(4,tansaction.roomId);
			smt.setString(5,tansaction.gamePlayId);
			smt.setDouble(6,tansaction.amount);
			smt.setDouble(7,tansaction.lastBalance);
			smt.setString(8,tansaction.ip);
			smt.executeUpdate();
			connection.close();
		} catch (Exception e)
		{
			e.printStackTrace();
			addDatabaseErrorLog("addBalanceTransaction", e.getMessage());
			try
			{
				connection.close();
			} catch (SQLException e1)
			{
				e1.printStackTrace();
			}
		}
	}
	
	public void addBuyJackpot(BuyJackpot buyJackpot)
	{
		Connection connection = getConnection();
		try
		{
			PreparedStatement smt = connection.prepareStatement("{call dbo.BuyJackpot(?,?,?,?,?,?,?)}");
			smt.setString(1,buyJackpot.gamePlayId);
			smt.setString(2,buyJackpot.roomId);
			smt.setString(3,buyJackpot.companyId);
			smt.setString(4,buyJackpot.playerId);
			smt.setString(5,buyJackpot.roundId);
			smt.setDouble(6,buyJackpot.buyAmount);
			smt.setDouble(7,buyJackpot.lastBalance);
			smt.executeUpdate();
			connection.close();
		} catch (Exception e)
		{
			e.printStackTrace();
			addDatabaseErrorLog("buyJackpot", e.getMessage());
			try
			{
				connection.close();
			} catch (SQLException e1)
			{
				e1.printStackTrace();
			}
		}
	}
	
	public void addJackpotWinner(JackpotWinner jackpotWinner)
	{
		Connection connection = getConnection();
		try
		{
			PreparedStatement smt = connection.prepareStatement("{call dbo.AddJackpotWinner(?,?,?,?,?,?,?,?,?,?)}");
			smt.setString(1,jackpotWinner.gamePlayId);
			smt.setString(2,jackpotWinner.companyId);
			smt.setString(3,jackpotWinner.playerId);
			smt.setString(4,jackpotWinner.roomId);
			smt.setString(5,jackpotWinner.round);
			smt.setString(6,jackpotWinner.cards);
			smt.setString(7,jackpotWinner.hand);
			smt.setDouble(8,jackpotWinner.buyAmount);
			smt.setDouble(9,jackpotWinner.rewardedAmount);
			smt.setDouble(10,jackpotWinner.lastBalance);
			smt.executeUpdate();
			connection.close();
		} catch (Exception e)
		{
			e.printStackTrace();
			addDatabaseErrorLog("addJackpotWinner", e.getMessage());
			try
			{
				connection.close();
			} catch (SQLException e1)
			{
				e1.printStackTrace();
			}
		}
	}
	
	private void addDatabaseErrorLog(String funtionName,String errorMessage)
	{
		Connection connection = getConnection();
		try
		{
			PreparedStatement smt = connection.prepareStatement("{call dbo.AddDatabaseErrorLog(?,?)}");
			smt.setString(1,funtionName);
			smt.setString(2,errorMessage);
			smt.executeUpdate();
			connection.close();
		} catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				connection.close();
			} catch (SQLException e1)
			{
				e1.printStackTrace();
			}
		}
	}
	
	private Connection getConnection()
	{
		try
		{
			DriverManager.registerDriver((Driver) Class.forName(driver).newInstance());
			return DriverManager.getConnection(OnServerReadyHandler.getGameConnection());
		} catch (Exception e)
		{
			e.printStackTrace();
			addDatabaseErrorLog("GetConnection", e.getMessage());
		}
		return null;
	}
}
