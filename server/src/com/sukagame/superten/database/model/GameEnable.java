package com.sukagame.superten.database.model;

public class GameEnable
{
	public String id;
	public String companyId;
	public String gameId;
	public String name;

	public GameEnable()
	{
		
	}
	
	public GameEnable(String id,String companyId,String gameId,String name)
	{
		this.id = id;
		this.companyId = companyId;
		this.gameId = gameId;
		this.name = name;
	}
}
