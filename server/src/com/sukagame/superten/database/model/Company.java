package com.sukagame.superten.database.model;

public class Company
{
	public String id;
	public String name;
	public String apiKey;
	public String url;
	public String apiUrl;
	public String callback;
	public boolean status;
	
	public Company()
	{
		
	}
	
	public Company(String id,String name,String apiKey,String url,String apiUrl,String callback,boolean status)
	{
		this.id = id;
		this.name = name;
		this.apiKey = apiKey;
		this.url = url;
		this.apiUrl = apiUrl;
		this.callback = callback;
		this.status = status;
	}
}
