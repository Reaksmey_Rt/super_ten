package com.sukagame.superten.database.model;

public class JackpotLimit
{
	public int id;
	public int roomTypeId;
	public double amount;
	
	public JackpotLimit()
	{
		
	}
	
	public JackpotLimit(int id, int roomTypeId,double amount)
	{
		this.id = id;
		this.roomTypeId = roomTypeId;
		this.amount = amount;
	}
}
