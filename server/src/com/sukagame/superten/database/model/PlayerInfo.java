package com.sukagame.superten.database.model;

public class PlayerInfo
{
	private String id;
	private String code;
	private String company_id;
	private String company_name;
	private String provider_name;
	private int avatar_id;
	private String referral_id;
	private String username;
	private String password;
	private String name;
	private boolean is_robot;
	private String salt; 
	private String pinCode;
	private double balance;
	

	public PlayerInfo(String id,String code,String company_id,String company_name,String providerName, Integer avatar_id,String referral_id, String username,String password, String name ,boolean is_robot,String salt,String pinCode, double balance)
	{
		this.id = id;
		this.code = code;
		this.company_id = company_id;
		this.company_name = company_name;
		this.provider_name = providerName;
		this.avatar_id = avatar_id;
		this.referral_id = referral_id;
		this.username = username;
		this.password = password;
		this.name = name;
		this.is_robot = is_robot;
		this.salt = salt;
		this.pinCode = pinCode;
		this.balance = balance;
	}
	
	public void setId(String id)
	{
		this.id =id;
	}
	
	public String getId()
	{
		return this.id;
	}
	
	public void setCode(String code)
	{
		this.code = code;
	}	
	
	public String getCode()
	{
		return this.code;
	}
	
	public void setCompanyId(String companyId)
	{
		this.company_id = companyId;
	}
	
	public String getCompanyId()
	{
		return this.company_id;
	}
	
	public void setCompanyName(String companyName)
	{
		this.company_name = companyName;
	}
	
	public  String getCompanyName()
	{
		return this.company_name;
	}
	
	public void setProviderName(String provider_name)
	{
		this.provider_name = provider_name;
	}
	
	public String getProviderName()
	{
		return this.provider_name;
	}
	
	public void setAvatar(int id)
	{
		this.avatar_id = id;
	}
	
	public int getAvatarId()
	{
		return this.avatar_id;
	}
	
	public void setReferralId(String referralId)
	{
		this.referral_id = referralId;
	}
	
	public String getReferralId()
	{
		return this.referral_id;
	}
	
	public void setUserName(String username)
	{
		this.username = username;
	}
	
	public void setIsRobot(boolean isRobot)
	{
		this.is_robot = isRobot;
	}
	
	public String getUsername()
	{
		return this.username;
	}
	
	public void setPassword(String password)
	{
		this.password = password;
	}
	
	public String getPassword()
	{
		return this.password;
	}
	
	public boolean isRobot()
	{
		return this.is_robot;
	}
	
 	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public void setSalt(String salt)
	{
		this.salt = salt;
	}
	
	public String getSalt()
	{
		return this.salt;
	}
	
	public void setBalance(double balance)
	{
		this.balance = balance;
	}
	
	public Double getBalance()
	{
		return this.balance;
	}
	
	public void setTemBalance(double balance)
	{
		this.balance = balance;
	}
	
	public Double getTemBalance()
	{
		return this.balance;
	}
	
	public String getPinCode()
	{
		return this.pinCode;
	}
	
	public void setPinCode(String pinCode)
	{
		this.pinCode = pinCode;
	}
}
