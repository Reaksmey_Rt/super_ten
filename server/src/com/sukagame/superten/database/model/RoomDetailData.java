package com.sukagame.superten.database.model;

public class RoomDetailData
{
	public int id;
	public int roomTypeId;
	public String name;
	public int roomLimitTypeId;
	public String limitName;
	public double minBalance;
	public double stake;
	public double buyInMinAmount;
	public double buyInMaxAmount;
	public double betMinAmount;
	
	public RoomDetailData() {}
	
	public RoomDetailData(int id,String name,String limitName,double minBalance,double stake)
	{
		this.id = id;
		this.name = name;
		this.limitName = limitName;
		this.minBalance = minBalance;
		this.stake = stake;
	}
	
	public RoomDetailData(int roomTypeId, String name,int roomLimitTypeId,String limitName,double minBalance,double buyInMinAmount,double buyInMaxAmount,double betMinAmount,double stake)
	{
		this.roomTypeId = roomTypeId;
		this.name = name;
		this.roomLimitTypeId = roomLimitTypeId;
		this.limitName = limitName;
		this.minBalance = minBalance;
		this.buyInMinAmount = buyInMinAmount;
		this.buyInMaxAmount = buyInMaxAmount;
		this.betMinAmount = betMinAmount;
		this.stake = stake;
	}
}
