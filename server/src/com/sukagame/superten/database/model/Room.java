package com.sukagame.superten.database.model;

import java.util.List;

//import com.sukagame.superten.database.model.JackpotLimit;

public class Room
{
	public String id;
	public int roomTypeId;
	public String roomTypeName;
	public int roomLimitTypeId;
	public String roomLimitTypeName;
	public String name;
	public String ownerId;
	public String password;
	public int status;
	public double stake;
	public boolean isStatic;
	public double minBalance;
	public double buyInMinAmount= 25000;
	public double buyInMaxAmount= 50000;
	public double betMinAmout = 200;
	public int minPlayer = 2;
	public int maxPlayer = 8;
	
	public boolean isCustomRoom;
	public List<JackpotLimit> jackpotLimits;
	public String round;
	
	public Room() {}
	
	public Room(String id,int roomTypeId,int roomLimitTypeId,String name,String ownerId, String password,int status,double stake,boolean isStatic,double minBalance)
	{
		this.id = id;
		this.roomTypeId = roomTypeId;
		this.roomLimitTypeId = roomLimitTypeId;
		this.name = name;
		this.password = password;
		this.status = status;
		this.stake = stake;
		this.ownerId = ownerId;
		this.isStatic = isStatic;
		this.minBalance = minBalance;
	
	}
	
	public Room(String id,int roomTypeId,String roomTypeName, int roomLimitTypeId,String roomLimitTypeName, String name,
			String ownerId, String password,int status,double stake, boolean isStatic,double minBalance,double minBuyIn,
			double maxBuyIn,double betMinAmt,boolean isCustomRoom,String round)
	{
		this.id = id;
		this.roomTypeId = roomTypeId;
		this.roomTypeName = roomTypeName;
		this.roomLimitTypeId = roomLimitTypeId;
		this.roomLimitTypeName = roomLimitTypeName;
		this.name = name;
		this.password = password;
		this.status = status;
		this.stake = stake;
		this.ownerId = ownerId;
		this.isStatic = isStatic;
		this.minBalance = minBalance;
		this.buyInMinAmount = minBuyIn;
		this.buyInMaxAmount = maxBuyIn;
		this.betMinAmout = betMinAmt;
		this.isCustomRoom = isCustomRoom;
		this.round = round;
	}
}
