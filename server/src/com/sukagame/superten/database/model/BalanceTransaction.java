package com.sukagame.superten.database.model;

public class BalanceTransaction
{
	public String id;
	public String companyId;
	public String playerId;
	public String roomId;
	public String gamePlayId;
	public double amount;
	public double lastBalance;
	public String ip ="";
	
	public BalanceTransaction() {}
	
	public BalanceTransaction(String id,String companyId, String playerId,String roomId,String gamePlayId,double amount,double lastBalance,String ip)
	{
		this.id = id;
		this.companyId = companyId;
		this.playerId = playerId;
		this.roomId = roomId;
		this.gamePlayId = gamePlayId;
		this.amount = amount;
		this.lastBalance = lastBalance;
		this.ip = ip;
	}
}
