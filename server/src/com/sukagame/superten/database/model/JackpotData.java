package com.sukagame.superten.database.model;

public class JackpotData
{
    public int jackpotId = -1;
    public double jackpotAmount = 0;
    public boolean isBuyJackpot = false;
    public double jackpotRewardedAmount = 0;

    public JackpotData(){}

    public JackpotData(int id,double amount,boolean isBuy)
    {
        this.jackpotId = id;
        this.jackpotAmount = amount;
        this.isBuyJackpot = isBuy;
    }
}
