package com.sukagame.superten.database.model;

public class PlayBetResult
{
	public String gamePlayId;
	public String companyId;
	public String companyName;
	public String playerId;
	public String username;
	public boolean isRobot;
	public String roomId;
	public String roomName;
	public String round;
	public String card;
	public int isWinSpecial;
	public String points;
	public String handAmounts;
	public int totalPoint;
	public double stake;
	public double totalBet;
	public String hand;
	public String handStatus;
	public double turnover;
	public double prize;
	public String status;
	public double amount;
	public double fee;
	public double shareCapital;
	public double commission;
	public double jackpotBuyAmount;
	public double jackpotWinAmount;
	public double lastBalance;
	public String providerName;

	public PlayBetResult() {}
	
	public PlayBetResult(String gamePlayId,
						String companyId, 
						String companyName,
						String playerId,
						String username,
						Boolean isRobot,
						String roomId,
						String roomName,
						String round,
						String card,
						Integer isWinSpecial,
						String points,
						String handAmount,
						int totalPoint,
						double stake,
						double totalBet,
						String hand,
						String handStatus,
						double turnover,
						double prize,
						String status,
						double amount,
						double fee,
						double shareCapital,
						double commission,
						double jackpotBuyAmount,
						double jackpotWinAmount,
						double lastBalance)
	{
		this.gamePlayId =gamePlayId;
		this.companyId = companyId;
		this.companyName = companyName;
		this.playerId = playerId;
		this.username = username;
		this.isRobot = isRobot;
		this.roomId = roomId;
		this.roomName = roomName;
		this.round = round;
		this.card = card;
		this.isWinSpecial = isWinSpecial;
		this.points = points;
		this.handAmounts = handAmount;
		this.totalPoint = totalPoint;
		this.stake = stake;
		this.totalBet = totalBet;
		this.hand = hand;
		this.handStatus = handStatus;
		this.turnover = turnover;
		this.prize = prize;
		this.status = status;
		this.amount = amount;
		this.fee = fee;
		this.shareCapital = shareCapital;
		this.commission = commission;
		this.jackpotBuyAmount = jackpotBuyAmount;
		this.jackpotWinAmount = jackpotWinAmount;
		this.lastBalance = lastBalance;
	}
}
