package com.sukagame.superten.database.model;

import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class RoomType
{
	public int id;
	public String name;
	
	public RoomType() {}
	
	public RoomType(int id, String name)
	{
		this.id = id;
		this.name = name;
	}
	
	public ISFSObject toSFSObject()
	{
		ISFSObject param = new SFSObject();
		param.putInt("id", id);
		param.putUtfString("name", name);
		return param;
	}
}
