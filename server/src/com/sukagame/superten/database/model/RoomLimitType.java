package com.sukagame.superten.database.model;

import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class RoomLimitType
{
	public int id;
	public String name;
	public double minBalance;
	public double stake;
	
	public RoomLimitType() {}
	
	public RoomLimitType(int id, String name,double minBalance,double stake)
	{
		this.id = id;
		this.name = name;
		this.minBalance = minBalance;
		this.stake = stake;
	}
	
	public ISFSObject toSFSObject()
	{
		ISFSObject param = new SFSObject();
		param.putInt("id", id);
		param.putUtfString("name", name);
		param.putDouble("min_balance", minBalance);
		param.putDouble("card_price", stake);
		return param;
	}
}
