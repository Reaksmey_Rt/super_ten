package com.sukagame.superten.database.model;

public class Maintenance
{
	public int id;
	public String gameId;
	public String companyId;
	public String gameName;
	public String startDate;
	public String endDate;
	public String description;
	
	public Maintenance()
	{
		
	}
	
	public Maintenance(int id, String gameId,String companyId,String gameName, String startDate,String endDate,String description)
	{
		this.id = id;
		this.gameId = gameId;
		this.companyId = companyId;
		this.gameName = gameName;
		this.startDate = startDate;
		this.endDate = endDate;
		this.description = description;
	}
}
