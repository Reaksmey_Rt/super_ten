package com.sukagame.superten.database.model;

public class JackpotWinner
{
	public String gamePlayId;
	public String companyId;
	public String playerId;
	public String roomId;
	public String round;
	public String cards;
	public String hand;
	public double buyAmount;
	public double rewardedAmount;
	public double lastBalance;
	
	public JackpotWinner()
	{
		
	}
	
	public JackpotWinner(String gamePlayId,String companyId,String playerId,String roomId,String round,String cards,String hand,double buyAmount,double rewardedAmount,double lastBalance)
	{
		this.gamePlayId = gamePlayId;
		this.companyId = companyId;
		this.playerId = playerId;
		this.roomId = roomId;
		this.round = round;
		this.cards =cards;
		this.hand = hand;
		this.buyAmount = buyAmount;
		this.rewardedAmount =rewardedAmount;
		this.lastBalance = lastBalance;
	}
}
