package com.sukagame.superten.database.model;

public class BalanceHistory
{
	public String id;
	public String gameId;
	public String companyId;
	public String playerId;
	public String type;
	public double amount;
	public double lastBalance;
	public String ip ="";
	
	public BalanceHistory() {}
	
	public BalanceHistory(String id,String gameId, String companyId, String playerId,String type,double amount,double lastBalance,String ip)
	{
		this.id = id;
		this.gameId = gameId;
		this.companyId = companyId;
		this.playerId = playerId;
		this.type = type;
		this.amount = amount;
		this.lastBalance = lastBalance;
		this.ip = ip;
	}
}
