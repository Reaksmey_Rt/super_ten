package com.sukagame.superten.database.model;

public class JackpotInfo 
{
	public String date; 
	public String username;
	public String hand;
	public double amount;
	
	public JackpotInfo() {}
	
	public JackpotInfo(String date,String username,String hand,double amount)
	{
		this.date = date;
		this.username = username;
		this.hand = hand;
		this.amount = amount;
	}
}
