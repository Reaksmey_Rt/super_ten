package com.sukagame.superten.zone.event;

import java.util.List;

import com.sukagame.global.GameManager;
import com.sukagame.superten.database.GameDatabaseManager;
import com.sukagame.superten.database.GameDatabaseManager.STORE_PROCEDURE;
import com.sukagame.superten.database.model.Room;
import com.sukagame.superten.util.GameRoom;
import com.sukagame.superten.util.Util;
import com.smartfoxserver.v2.api.ISFSApi;
import com.smartfoxserver.v2.core.ISFSEvent;
import com.smartfoxserver.v2.entities.Zone;
import com.smartfoxserver.v2.exceptions.SFSException;

public class OnServerReadyHandler extends BaseSukaZoneServerEventHandler
{
	public static String serverName = "";
	public static String userName = "";
	public static String password = "";
	public static String sukagameDatabaseName = "sukagame";
	public static String gameDatabaseName = "superten";
	
	public OnServerReadyHandler(Zone paramZone, ISFSApi paramApi)
	{
		zone = paramZone;
		api = paramApi;
		
		GameManager gameManager = GameManager.getInstance();
		gameManager.zone = paramZone;
		gameManager.api = paramApi;
		
		serverName = (String)paramZone.getProperty("server_name");
		userName = (String)paramZone.getProperty("username");
		password = (String)paramZone.getProperty("password");
	}
	
	@Override
	public void handleServerEvent(ISFSEvent event) throws SFSException
	{
		super.handleServerEvent(event);
		List<Room> rooms = new GameDatabaseManager().getRooms();
		GameManager.getInstance().roomsDatabase = rooms;
		for (Room room : rooms)
		{
			int noId = new GameDatabaseManager().getLatestId(STORE_PROCEDURE.GET_ROOM_LATEST_ID);
			room.id = Util.getFormatNumber("R", noId);
			room.jackpotLimits = new GameDatabaseManager().getJackpotLimits(room.roomTypeId);
			GameRoom.create(room, GameManager.getInstance().api,GameManager.getInstance().zone, user);
			return;
		}
	}
	
	public static String getSukagameConnection()
	{
		return "jdbc:sqlserver://"+serverName+":1433;database="+sukagameDatabaseName+";user="+userName+"; password="+password+";";
	}
	
	public static String getGameConnection()
	{
		return "jdbc:sqlserver://"+serverName+":1433;database="+gameDatabaseName+";user="+userName+"; password="+password+";";
	}
	
	public static String getProviderConnection(String providerDatabaseName)
	{
		return "jdbc:sqlserver://"+serverName+":1433;database="+providerDatabaseName+";user="+userName+"; password="+password+";";
	}
}
