package com.sukagame.superten.zone.event;

import com.smartfoxserver.v2.api.ISFSApi;
import com.smartfoxserver.v2.core.ISFSEvent;
import com.smartfoxserver.v2.core.ISFSEventListener;
import com.smartfoxserver.v2.core.SFSEventParam;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.Zone;
import com.smartfoxserver.v2.exceptions.SFSException;

public class BaseSukaZoneServerEventHandler implements ISFSEventListener
{
	public Zone zone;
	public ISFSApi api;
	public User user = null;
	
	@Override
	public void handleServerEvent(ISFSEvent event) throws SFSException
	{
		user = (User) event.getParameter(SFSEventParam.USER);
	}
}
