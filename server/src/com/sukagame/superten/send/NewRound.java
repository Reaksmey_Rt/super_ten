package com.sukagame.superten.send;

import com.sukagame.global.Key;
import com.sukagame.global.RoomCommand;
import com.sukagame.superten.room.RoomExtension;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class NewRound extends BaseSend
{
	public NewRound(RoomExtension roomExtension)
	{
		super(roomExtension);
	}
	
	public void send(Integer roundCount)
	{
		ISFSObject param = new SFSObject();
		param.putUtfString(Key.KEY_ROOM_STATE, stateName);
		param.putInt(Key.KEY_ROOM_ROUND_COUNT, roundCount);
		send(RoomCommand.CMD_NEW_ROUND, param.toJson());
		print(this.getClass().getName()+ " : " +param.toJson());
	}
}
