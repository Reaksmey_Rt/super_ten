package com.sukagame.superten.send;

import com.sukagame.global.Key;
import com.sukagame.global.RoomCommand;
import com.sukagame.superten.gamelogic.Player;
import com.sukagame.superten.room.RoomExtension;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class Jackpot extends BaseSend
{
	public Jackpot(RoomExtension roomExtension)
	{
		super(roomExtension);
	}
	
	public void respondProgressiveJackpot(double balance)
	{
		ISFSObject param = new SFSObject();
		param.putDouble("balance", balance);
		send("CMD_RESPOND_PROGRESSIVE_JACKPOT_BALANCE", param.toJson());
	}

	public void send()
	{
		ISFSObject param = new SFSObject();
		param.putUtfString(Key.KEY_ROOM_STATE, stateName);
		send(RoomCommand.CMD_BUY_JACKPOT, param.toJson());
		print(this.getClass().getName()+ " : " +param.toJson());
	}

	public void sendBuyJackpot(Player player , int jackpotId,int nextId, double amount, String message)
	{
		ISFSObject param = new SFSObject();
		param.putUtfString(Key.KEY_ROOM_STATE, stateName);
		param.putInt(Key.KEY_ROOM_PLAYER_SERVER_SEAT, player.getServerSeat());
		param.putInt("id", jackpotId);
		param.putInt("next_id",nextId);
		param.putDouble("amount",amount);
		param.putUtfString("message",message);
		send(RoomCommand.CMD_BUY_GAME_JACKPOT, param.toJson(),player.getUser());
		print(this.getClass().getName()+ " : " +param.toJson());
	}
}
