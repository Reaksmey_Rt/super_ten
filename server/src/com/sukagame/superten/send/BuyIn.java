package com.sukagame.superten.send;

import com.smartfoxserver.v2.entities.data.SFSObject;
import com.sukagame.global.Key;
import com.sukagame.global.RoomCommand;

import com.sukagame.superten.database.model.Room;
import com.sukagame.superten.gamelogic.Player;
import com.sukagame.superten.room.RoomExtension;

public class BuyIn extends BaseSend {

	public BuyIn(RoomExtension roomExtension) {
		super(roomExtension);

	}

	public void SendBuyIn( Player player, Room room, Boolean active) {
		SFSObject params = new SFSObject();
		params.putBool(Key.ACTIVE, active);

		if (active) {
			params.putDouble(Key.KEY_ROOM_MIN_BUY_IN, room.buyInMinAmount);
			params.putDouble(Key.KEY_ROOM_MAX_BUY_IN, room.buyInMaxAmount);
			params.putDouble(Key.KEY_ROOM_PLAYER_BALANCE, player.getBalance());
		}

		params.putUtfString(Key.KEY_PLAYER_NAME,player.getName());
		
		send(RoomCommand.CMD_BUY_IN_PANEL_CHANGED, params.toJson(),player.getUser());
		//send(RoomCommand.CMD_BUY_IN_PANEL_CHANGED, params.toJson(),playerController.player_session.getUser());
	}
}
