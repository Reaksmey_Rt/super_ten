package com.sukagame.superten.send;

import com.sukagame.global.Key;
import com.sukagame.global.RoomCommand;
import com.sukagame.superten.gamelogic.Player;
import com.sukagame.superten.room.RoomExtension;

import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class General extends BaseSend
{
	public General(RoomExtension roomExtension)
	{
		super(roomExtension);
	}
	
	public void SendRespondLeavePending(Player player)
	{
		ISFSObject param = new SFSObject();
		param.putUtfString(Key.KEY_ROOM_STATE, stateName);
		param.putBool("is_leave_pending", player.getIsRequestLeave());
		param.putUtfString("message", "You will leave when end game!.");
		send(RoomCommand.CMD_RESPOND_LEAVE_PENDING, param.toJson(),player.getUser());
		print( " ___ " +param.toJson());
	}
	 
	public void sendUpdateState(String cmdString) {
		ISFSObject param = new SFSObject();
		param.putUtfString(Key.KEY_ROOM_STATE, stateName);	
		send(cmdString, param.toJson());
		print(  "______"+ cmdString + "  :  "+param.toJson());
	}
	
}
