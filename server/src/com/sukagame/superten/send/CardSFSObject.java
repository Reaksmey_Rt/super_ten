package com.sukagame.superten.send;

import java.util.List;

import com.sukagame.global.Key;
import com.sukagame.global.RoomCommand;
import com.sukagame.superten.gamelogic.Player;
import com.sukagame.superten.room.RoomExtension;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class CardSFSObject extends BaseSend
{
	public CardSFSObject(RoomExtension roomExtension)
	{
		super(roomExtension);
	}
	
	public void sendDealCardByPlayer(Player player)
	{
		ISFSObject param = new SFSObject();
		param.putUtfString(Key.KEY_ROOM_STATE, stateName);
		param.putSFSArray(Key.KEY_ROOM_PLAYERS, playerController.toSFSObjectDealCard(player.getServerSeat()));
		send(RoomCommand.CMD_DEAL_CARD, param.toJson(),player.getUser());
		print(RoomCommand.CMD_DEAL_CARD +" :::::: " +param.toJson());
	}
	
	public void sendDealCardAllPlayersNoCard()
	{
		//send to all spectator
		ISFSObject param = new SFSObject();
		param.putUtfString(Key.KEY_ROOM_STATE, stateName);
		param.putSFSArray(Key.KEY_ROOM_PLAYERS, playerController.toSFSObjectDealCardAll());
		sendSpec(RoomCommand.CMD_DEAL_CARD, param.toJson());
		print(this.getClass().getName()+ " : " +param.toJson());
		
		//send to none active players
		List<Player> members = playerController.getSeatedPlayerNotPlayingYet();
		for (Player player : members)
		{
			send(RoomCommand.CMD_DEAL_CARD, param.toJson(),player.getUser());
		}
	}
	
	public void sendRevealCardByPlayer(int card_amt) {
		
		ISFSObject param = new SFSObject();
		param.putUtfString(Key.KEY_ROOM_STATE, stateName);
		//param.putSFSArray(Key.KEY_ROOM_PLAYERS, playerController.toSFSObjectDealCard(player.getServerSeat()));
		param.putInt(Key.KEY_ROOM_CARD_COUNT,card_amt);
		send(RoomCommand.CMD_REVEAL_CARD, param.toJson());
		print("CMD_REVEAL_CARD :______________________ "+this.getClass().getName()+ " : " +param.toJson());
	}
	
	public void sendOpenCardByPlayer(int card_amt) {
		
		ISFSObject param = new SFSObject();
		param.putUtfString(Key.KEY_ROOM_STATE, stateName);		
		param.putInt(Key.KEY_ROOM_CARD_COUNT,card_amt);
		send(RoomCommand.CMD_OPEN_CARD, param.toJson());
		print("CMD_OPEN_CARD :______________________ "+" : " +param.toJson());
	}
	


	
}
