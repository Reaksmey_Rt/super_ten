package com.sukagame.superten.send;

import com.sukagame.global.Key;
import com.sukagame.global.RoomCommand;
import com.sukagame.superten.gamelogic.Player;
import com.sukagame.superten.room.RoomExtension;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class Confirm extends BaseSend
{
	public Confirm(RoomExtension roomExtension)
	{
		super(roomExtension);
	}
	
	public void send(Player player)
	{
		ISFSObject param = new SFSObject();
		param.putUtfString(Key.KEY_ROOM_STATE, stateName);
		param.putInt(Key.KEY_ROOM_PLAYER_SERVER_SEAT, player.getServerSeat());
		param.putBool("is_confirm", true);
		send(RoomCommand.CMD_RESPOND_CONFIRM_CARDS_RANKED, param.toJson());
		print(this.getClass().getName()+ " : " +param.toJson());
	}
}
