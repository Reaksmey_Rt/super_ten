package com.sukagame.superten.send;

import com.sukagame.global.Key;
import com.sukagame.global.RoomCommand;
import com.sukagame.superten.gamelogic.Player;
import com.sukagame.superten.room.RoomExtension;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class UpdateBalance extends BaseSend
{
	public UpdateBalance(RoomExtension roomExtension)
	{
		super(roomExtension);
	}
	
	public void send(Player player)
	{
		ISFSObject param = new SFSObject();
		param.putUtfString(Key.KEY_ROOM_STATE, stateName);
		param.putInt(Key.KEY_ROOM_PLAYER_SERVER_SEAT, player.getServerSeat());
		param.putDouble(Key.KEY_ROOM_PLAYER_BALANCE, player.getBalance());
		send(RoomCommand.CMD_UPDATE_BALANCE, param.toJson());
		print(this.getClass().getName()+ " : " +param.toJson());
	}

	public void sendSpecialWin(Player player,double amount, double fee)
	{
		ISFSObject param = new SFSObject();
		param.putUtfString(Key.KEY_ROOM_STATE, stateName);
		param.putInt(Key.KEY_ROOM_PLAYER_SERVER_SEAT, player.getServerSeat());
		param.putDouble("amount", amount);
		param.putDouble("fee", fee);
		send(RoomCommand.CMD_WIN_LOST_SPECIAL_AMOUNT, param.toJson());
		print(this.getClass().getName()+ " : " +param.toJson());
	}
}
