package com.sukagame.superten.send;

import com.sukagame.global.Key;
import com.sukagame.global.RoomCommand;
import com.sukagame.superten.gamelogic.Player;
import com.sukagame.superten.gamelogic.Table;
import com.sukagame.superten.room.RoomExtension;
import com.sukagame.superten.util.GameUtil;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

import java.util.List;

public class Result extends BaseSend
{
	public Result(RoomExtension roomExtension)
	{
		super(roomExtension);
	}
	
	public void send(Integer gameCount,Table table,String handName)
	{
		ISFSObject param = new SFSObject();
		param.putUtfString(Key.KEY_ROOM_STATE, stateName);
		param.putInt(Key.KEY_ROOM_GAME_COUNT, gameCount);
		param.putSFSArray(Key.KEY_ROOM_PLAYERS,playerController.toSFSObjectResult(handName));
		send(RoomCommand.CMD_RESULT, param.toJson());
		print(this.getClass().getName()+ " : " +param.toJson());
	}

	public void send(Integer gameCount,Player player)
	{
		ISFSObject param = new SFSObject();
		param.putUtfString(Key.KEY_ROOM_STATE, stateName);
		param.putInt(Key.KEY_ROOM_GAME_COUNT, gameCount);
		param.putInt(Key.KEY_ROOM_PLAYER_SERVER_SEAT, player.getServerSeat());
		send("CMD_RESULT_SPECIAL", param.toJson());
		print(this.getClass().getName()+ " : " +param.toJson());
	}

	public void send(Integer gameCount,String handName)
	{
		ISFSObject param = new SFSObject();
		param.putUtfString(Key.KEY_ROOM_STATE, stateName);
		param.putInt(Key.KEY_ROOM_GAME_COUNT, gameCount);
		param.putSFSArray(Key.KEY_ROOM_PLAYERS,playerController.toSFSObjectResult(handName));
		send(RoomCommand.CMD_RESULT_TOTAL, param.toJson());
		print(this.getClass().getName()+ " : " +param.toJson());
	}

	public void send(Integer gameCount, String handName, User user)
	{
		ISFSObject param = new SFSObject();
		param.putUtfString(Key.KEY_ROOM_STATE, stateName);
		param.putInt(Key.KEY_ROOM_GAME_COUNT, gameCount);
		param.putSFSArray(Key.KEY_ROOM_PLAYERS,playerController.toSFSObjectResult(handName));
		send(RoomCommand.CMD_RESULT_TOTAL, param.toJson(),user);
		print(this.getClass().getName()+ " : " +param.toJson());
	}

	public void send(Integer gameCount, int shooterSeatId, List<Integer> seateds)
	{
		ISFSObject param = new SFSObject();
		param.putUtfString(Key.KEY_ROOM_STATE, stateName);
		param.putInt(Key.KEY_ROOM_GAME_COUNT, gameCount);
		param.putInt(Key.KEY_ROOM_PLAYER_SERVER_SEAT, shooterSeatId);
		param.putIntArray(Key.KEY_ROOM_PLAYER_SERVER_SEATS, seateds);
		send(RoomCommand.CMD_RESULT_SHOOT_ANIMATION, param.toJson());
		print(this.getClass().getName()+ " : " +param.toJson());
	}

	public void sendSpecialWin2Players(Integer gameCount,Player player)
	{
		ISFSObject param = new SFSObject();
		param.putUtfString(Key.KEY_ROOM_STATE, stateName);
		param.putInt(Key.KEY_ROOM_GAME_COUNT, gameCount);
		param.putInt(Key.KEY_ROOM_PLAYER_SERVER_SEAT, player.getServerSeat());
		param.putSFSArray(Key.KEY_ROOM_PLAYER_CARDS, GameUtil.toSFSObject(player.getHand().getCards()));
		send("CMD_RESULT_WIN_SPECIAL_TWO_PLAYER", param.toJson());
		print(this.getClass().getName()+ " : " +param.toJson());
	}
}
