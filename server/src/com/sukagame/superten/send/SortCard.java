package com.sukagame.superten.send;

import com.sukagame.global.Key;
import com.sukagame.global.RoomCommand;
import com.sukagame.superten.gamelogic.Player;
import com.sukagame.superten.room.RoomExtension;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class SortCard extends BaseSend
{
	public SortCard(RoomExtension roomExtension)
	{
		super(roomExtension);
	}
	
	public void send(Player player)
	{
		ISFSObject param = new SFSObject();
		param.putUtfString(Key.KEY_ROOM_STATE, stateName);
		param.putSFSObject(Key.KEY_ROOM_PLAYER, player.toSFSObjectSelf());
		send(RoomCommand.CMD_SORT_CARD, param.toJson(),player.getUser());
		print(this.getClass().getName()+ " : " +param.toJson());
	}
}
