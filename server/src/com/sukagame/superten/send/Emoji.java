package com.sukagame.superten.send;

import com.sukagame.global.Key;
import com.sukagame.global.RoomCommand;
import com.sukagame.superten.gamelogic.Player;
import com.sukagame.superten.room.RoomExtension;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class Emoji extends BaseSend
{
	public Emoji(RoomExtension roomExtension)
	{
		super(roomExtension);
	}
	
	public void send(Player player, int emojiId)
	{
		ISFSObject param = new SFSObject();
		param.putUtfString(Key.KEY_ROOM_STATE, stateName);
		param.putInt(Key.KEY_ROOM_PLAYER_SERVER_SEAT, player.getServerSeat());
		param.putInt("id", emojiId);
		send(RoomCommand.CMD_RESPOND_EMOJI, param.toJson());
		print(this.getClass().getName()+ " : " +param.toJson());
	}
}
