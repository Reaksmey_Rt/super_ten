package com.sukagame.superten.send;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.sukagame.superten.gamelogic.Player;
import com.sukagame.superten.room.RoomExtension;

public class PlayerJoin extends BaseSend
{
	public PlayerJoin(RoomExtension roomExtension)
	{
		super(roomExtension);
	}

	public void sendSelf(Player self)
	{
		ISFSObject param = new SFSObject();
		param.putUtfString("state", stateName);
		param.putSFSObject("player",self.toSFSObjectSelf());
		send("CMD_PLAYER_INFO", param.toJson(),self.getUser());
		print("CMD_PLAYER_INFO : "+ this.getClass().getName()+ " : " +param.toJson());
	}

	public void  sendAll()
	{
		ISFSObject param = new SFSObject();
		param.putUtfString("state", stateName);
		param.putSFSArray("players", playerController.toSFSArrayActivePlayers());
		send("CMD_ALL_PLAYERS_INFO", param.toJson());
		print("CMD_ALL_PLAYERS_INFO : "+this.getClass().getName()+ " : " +param.toJson());
	}

	public void sendAllToSelf(User user)
	{
		ISFSObject param = new SFSObject();
		param.putUtfString("state", stateName);
		param.putSFSArray("players", playerController.toSFSArrayActivePlayers());
		send("CMD_ALL_PLAYERS_INFO", param.toJson(),user);
		print("CMD_ALL_PLAYERS_INFO : "+this.getClass().getName()+ " : " +param.toJson());
	}

	//===============================operation====================================//
}
