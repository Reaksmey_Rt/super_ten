package com.sukagame.superten.send;

import com.sukagame.global.Key;
import com.sukagame.global.RoomCommand;
import com.sukagame.superten.room.RoomExtension;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class NewGame extends BaseSend
{
	public NewGame(RoomExtension roomExtension)
	{
		super(roomExtension);
	}
	
	public void send(Integer playerCount,Integer roundCount,Integer gameCount)
	{
		ISFSObject param = new SFSObject();
		param.putUtfString(Key.KEY_ROOM_STATE, stateName);
		param.putInt(Key.KEY_ROOM_PLAYER_COUNT, playerCount);
		param.putInt(Key.KEY_ROOM_ROUND_COUNT, roundCount);
		param.putInt(Key.KEY_ROOM_GAME_COUNT, gameCount);
		send(RoomCommand.CMD_NEW_GAME, param.toJson());
		print("New Game :: "+ this.getClass().getName() +param.toJson());
	}
}
