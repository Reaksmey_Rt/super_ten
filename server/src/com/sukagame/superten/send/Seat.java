package com.sukagame.superten.send;

import com.sukagame.global.Key;
import com.sukagame.global.RoomCommand;
import com.sukagame.superten.gamelogic.Player;
import com.sukagame.superten.room.RoomExtension;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class Seat extends BaseSend
{
	public Seat(RoomExtension roomExtension)
	{
		super(roomExtension);
	}
	
	public void sendMessage(Player player,String message)
	{
		ISFSObject param = new SFSObject();
		param.putUtfString(Key.KEY_ROOM_STATE, stateName);
		param.putUtfString("message", message);
		param.putInt(Key.KEY_ROOM_PLAYER_SERVER_SEAT, player.getServerSeat());
		send(RoomCommand.CMD_RESPOND_SEAT, param.toJson(),player.getUser());
		//player.setRequestSeat(-1);// reset request seat
		print(this.getClass().getName()+ " : " +param.toJson());
	}
	

}
