package com.sukagame.superten.send;

import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.sukagame.global.Key;
import com.sukagame.global.RoomCommand;
import com.sukagame.superten.gamelogic.Player;
import com.sukagame.superten.room.RoomExtension;

public class BettingTurn extends BaseSend{

	public BettingTurn(RoomExtension roomExtension) {
		super(roomExtension);
	}
	
	
	public void sendAllPlayersTurn(Player player) {
		
		ISFSObject param = new SFSObject();
		param.putUtfString(Key.KEY_ROOM_STATE, stateName);	
		param.putInt(Key.KEY_ROOM_TURN, player.getServerSeat());
		
		send(RoomCommand.CMD_BETTING, param.toJson());
		
		
		print(  "______"+ "  :  "+param.toJson());
	}
	

}
