package com.sukagame.superten.send;

import com.sukagame.global.Key;
import com.sukagame.global.RoomCommand;
import com.sukagame.superten.room.RoomExtension;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class Waiting extends BaseSend
{
	public Waiting(RoomExtension roomExtension)
	{
		super(roomExtension);
	}
	
	public void send(Integer time,Integer maxTime)
	{
		ISFSObject param = new SFSObject();
		param.putUtfString(Key.KEY_ROOM_STATE, stateName);
		param.putInt(Key.KEY_ROOM_TIME, time);
		param.putInt(Key.KEY_ROOM_MAX_TIME, maxTime);
		send(RoomCommand.CMD_WAITING, param.toJson());
		//print( "      CMD_WAITING: " +param.toJson());
	}
	
	public void waitMili(int mili)
	{
		ISFSObject param = new SFSObject();
		param.putUtfString(Key.KEY_ROOM_STATE, stateName);
		param.putInt(Key.KEY_ROOM_WAIT_MILI, mili);
		send(RoomCommand.CMD_WAITING, param.toJson());
		//print( "      CMD_WAITING: " +param.toJson());
	}
}
