package com.sukagame.superten.send;

import com.sukagame.global.Key;
import com.sukagame.global.RoomCommand;
import com.sukagame.superten.gamelogic.Player;
import com.sukagame.superten.room.RoomExtension;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class Stand extends BaseSend
{
	public Stand(RoomExtension roomExtension)
	{
		super(roomExtension);
	}
	
	public void sendMessage(Player player,boolean isStand)
	{
		ISFSObject param = new SFSObject();
		param.putUtfString(Key.KEY_ROOM_STATE, stateName);
		param.putBool(Key.KEY_ROOM_PLAYER_IS_PLAYING, player.getIsPlaying());
		param.putBool("is_stand", isStand);
		param.putBool("is_request_stand", player.getIsRequestStand());
		param.putUtfString("message", "will stand when end game!");
		param.putInt(Key.KEY_ROOM_PLAYER_SERVER_SEAT, player.getServerSeat());
		send(RoomCommand.CMD_RESPOND_STAND, param.toJson(),player.getUser());
		print(this.getClass().getName()+ " : " +param.toJson());
	}
}
