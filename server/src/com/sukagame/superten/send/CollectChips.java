
package com.sukagame.superten.send;

import com.sukagame.global.Key;
import com.sukagame.global.RoomCommand;
import com.sukagame.superten.room.RoomExtension;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class CollectChips extends BaseSend
{
	public CollectChips(RoomExtension roomExtension)
	{
		super(roomExtension);
	}
	
	public void sendAllPlayersBetting()
	{
		ISFSObject param = new SFSObject();
		param.putUtfString(Key.KEY_ROOM_STATE, stateName);			
		param.putSFSArray("players", playerController.toSFSArrayActivePlayers());
		send(RoomCommand.CMD_COLLECT_CHIP, param.toJson());
		print("CMD_COLLECT_CHIP ::::::::::> " + param.toJson() + "   CLASS : "+ this.getClass().getName() );
	}
	
	public void sendPlayerBintting(Integer chip_value)
	{
		ISFSObject param = new SFSObject();
		param.putUtfString(Key.KEY_ROOM_STATE, stateName);		
		param.putInt(Key.KEY_BET, chip_value);
		send(RoomCommand.CMD_COLLECT_CHIP, param.toJson());
		print("CMD_COLLECT_CHIP ::::::::::> " + param.toJson() + "   CLASS : "+ this.getClass().getName() );
	}
}