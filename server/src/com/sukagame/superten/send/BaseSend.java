package com.sukagame.superten.send;

import com.smartfoxserver.v2.entities.User;
import com.sukagame.superten.gamelogic.PlayerController;
import com.sukagame.superten.room.RoomExtension;

public class BaseSend
{
	public RoomExtension roomExtension;
	public String stateName;
	public PlayerController playerController;
	
	public BaseSend(RoomExtension roomExtension)
	{
		playerController = roomExtension.getPlayerController();
		stateName = roomExtension.getGameTemplate().getStateName();
		this.roomExtension = roomExtension;
	}
	
	public void send(String cmd, String json)
	{
		roomExtension.send(cmd, json);
	}
	
	public void send(String cmd, String json, User user)
	{
		roomExtension.send(cmd, json,user);
	}
	
	public void sendSpec(String cmd, String json)
	{
		roomExtension.sendSpec(cmd, json);
	}
	
	public void sendPlayer(String cmd, String json)
	{
		roomExtension.sendPlayer(cmd, json);
	}
	
	public void print(String message)
	{
		System.out.println(message);
	}
}
