package com.sukagame.superten.error;

import java.util.List;

import com.smartfoxserver.v2.exceptions.IErrorCode;
import com.smartfoxserver.v2.exceptions.SFSErrorData;

public class CustomError extends SFSErrorData
{

	public CustomError(IErrorCode code)
	{
		super(code);
	}

	@Override
	public void addParameter(String parameter)
	{
		super.addParameter(parameter);
	}

	@Override
	public IErrorCode getCode()
	{
		return super.getCode();
	}

	@Override
	public List<String> getParams()
	{
		return super.getParams();
	}

	@Override
	public void setCode(IErrorCode code)
	{
		super.setCode(code);
	}

	@Override
	public void setParams(List<String> params)
	{
		super.setParams(params);
	}

}
