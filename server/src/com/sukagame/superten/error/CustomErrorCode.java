package com.sukagame.superten.error;

import com.smartfoxserver.v2.exceptions.IErrorCode;

public enum CustomErrorCode implements IErrorCode
{
	BAD_DATA(200),
	BAD_SESSION(201),
	NOT_ENOUGH_MONEY_TO_JOIN_ROOM(202),
	ROOM_DOES_NOT_EXIST(203),
	ROOM_FULL(204),
	ROOM_JOIN_ERROR(205),
	ROOM_INCORRECT_PASSWORD(206),
	ROOM_CREATE_ERROR(207),
	ROOM_BAD_NAME(208),
	NOT_ENOUGH_MONEY_TO_CREATE_ROOM(209),
	PASSWORD_INCORRECT(210),
	PIN_INCORRECT(211),
	All_ROOM_FULL(212),
	ROOM_REQUIRE_PASSWORD(213),
	NOT_ENOUGH_MONEY_TO_BUY_IN(214),
	UNKNOWN(215);
	
	private  int code;
	
	private CustomErrorCode(int code)
	{
		this.code = code;
	}
	
	public void setCode(int code)
	{
		this.code = code;
	}
	
	@Override
	public short getId()
	{
		return (short)this.code;
	}

}
