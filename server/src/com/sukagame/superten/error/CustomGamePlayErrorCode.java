package com.sukagame.superten.error;

import com.smartfoxserver.v2.exceptions.IErrorCode;

public enum CustomGamePlayErrorCode implements IErrorCode
{
	CARDS_INVALID(100);
	
	private  int code;
	
	private CustomGamePlayErrorCode(int code)
	{
		this.code = code;
	}
	
	public void setCode(int code)
	{
		this.code = code;
	}
	
	@Override
	public short getId()
	{
		return (short)this.code;
	}
}
